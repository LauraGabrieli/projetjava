/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class ResAccueilController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    // Tableau
    @FXML
    private TableView table;
    @FXML
    private TableColumn columnMention;
    private ObservableList<Mention> tableauMention = FXCollections.observableArrayList();
    private ArrayList<Mention> listeMention = new ArrayList<>();

    // Trie
    @FXML
    private TextField textFieldMention;
    
    // Label Erreur
    @FXML
    private Label labelErr;
    
    // Boutons
    @FXML
    private Button buttonVider;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser les restaure
        restaureEtudiant = new ArrayList<>();
        restaureEnseignement = new ArrayList<>();
        restaureMention = new ArrayList<>();
        restaureParcours = new ArrayList<>();

        // Initialisation du tableau
        columnMention.setCellValueFactory(new PropertyValueFactory<>("nom"));

        table.setItems(null);

        // Autres
        tableAction();
        labelErr.setVisible(false);
    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * intialise les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * Initialiser le tableau avec toutes les mentions
     */
    public void initialiserTableauMention() {
        tableauMention.clear();
        for (Mention m : restaureMention) {
            tableauMention.add(m);
        }
        table.setItems(tableauMention);
    }

    /**
     * Afficher dans le tableau la mention donnée si elle existe
     * @param m 
     */
    public void afficherTableauTrie(Mention m) {
        tableauMention.clear();
        tableauMention.add(m);
        table.setItems(tableauMention);
    }

    /**
     * Récupère les enseignements en fonction d'une mention
     * @param ens
     * @param m
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourMention(ArrayList<Enseignement> ens, String m) {
        if ((m != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeM = new ArrayList<>();
            listeM.clear();
            listeM = Parcours.listeEnsMention(restaureParcours, m);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeM) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }
    
    /**
    * Trie les enseignements en fonction d'une mention
    */
    public void trieMention() {
        boolean trouver = false;
        labelErr.setVisible(false);
        String mention = textFieldMention.getText();
        mention = mention.toUpperCase();
        if (!mention.toUpperCase().equals("")) {
            for (Mention m : restaureMention) {
                if (mention.equals(m.getNom())) {
                    trouver = true;
                    afficherTableauTrie(m);
                }
            }
            if (!trouver) {
                labelErr.setVisible(true);
                initialiserTableauMention();
            }
        } else {
            initialiserTableauMention();
        }

    }

    /**
     * afficher les enseignements d'une mention
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Mention> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Mention mention = row.getItem();
                    try {
                        Stage source = (Stage) table.getScene().getWindow();
                        source.close();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/ResStat.fxml"));
                        Parent root = loader.load();

                        ResStatController pageResStat = loader.getController();
                        pageResStat.setRepertoire(repertoire);
                        pageResStat.setFichierEtudiant(fichierEtudiant);
                        pageResStat.setFichierEnseignement(fichierEnseignement);
                        pageResStat.setFichierMention(fichierMention);
                        pageResStat.setFichierParcours(fichierParcours);
                        pageResStat.initialiserLesRestaure();
                        pageResStat.setLabelNomMention(mention.getNom());
                        pageResStat.setMention(mention);
                        pageResStat.initialiserComboBox();
                        pageResStat.initialiserListe();

                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            return row;
        });
    }

    /**
     * Vide les comboBox et les textFields
     * Réinitialise les comboBox
     */
    public void buttonVider() {
        labelErr.setVisible(false);
        textFieldMention.clear();
        initialiserTableauMention();
    }

    /**
     * afficher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }
}
