/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.EtuParEns;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class ResStatController implements Initializable {

    private ArrayList<Enseignement> listeEnseignement = new ArrayList<>();
    private ObservableList<Enseignement> tableauEnseignement = FXCollections.observableArrayList();
    private Mention mention;
    private String parcours;
    private String ue;
    private String code;
    private int niveau; // utiliser pour afficher toutes les ue d'un niveau
    private String niv; // utiliser pour le trie
    private int n; // utiliser pour le trie
    private static final String date = "2020-2021";
    private static final String datePrev = "Prévisionnel : 2021-2022";

    private ObservableList<EtuParEns> tableauEtuParEns = FXCollections.observableArrayList();
    private ArrayList<EtuParEns> liste = new ArrayList<>();

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    // Tableau
    @FXML
    private TableView table;
    @FXML
    private TableColumn columnCode;
    @FXML
    private TableColumn columnNom;
    @FXML
    private TableColumn columnNbEtu;
    @FXML
    private TableColumn columnNbEtuPrev;

    // Button
    @FXML
    private Button buttonRetour;
    @FXML
    private Button buttonVider;
    @FXML
    private Button buttonAfficherParNiveau;
    @FXML
    private Button buttonAfficherTout;

    // Label
    @FXML
    private Label labelNomMention;
    @FXML
    private Label labelNiveau;

    // ComboBox
    @FXML
    private ComboBox ComboBoxListeParcours;
    @FXML
    private ComboBox comboBoxListeUE;
    @FXML
    private ComboBox comboBoxListeCodeUE;
    @FXML
    private ComboBox comboBoxNivTrie;

    @FXML
    private ComboBox comboBoxNiveau;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser les restaure
        restaureEtudiant = new ArrayList<>();
        restaureEnseignement = new ArrayList<>();
        restaureMention = new ArrayList<>();
        restaureParcours = new ArrayList<>();

        // Initialisation du tableau 
        columnCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));

        table.setItems(null);

        // Autres
        tableAction();
        this.niveau = 1;

    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * Modifier le label correspondant au nom de la mention
     * @param str 
     */
    public void setLabelNomMention(String str) {
        labelNomMention.setText("Liste des enseignements pour la mention : \n" + str);
    }

    /**
     * Modifier la mention
     * @param m 
     */
    public void setMention(Mention m) {
        mention = m;
    }

    /**
     * Récupérer le niveau par rapport au nom de l'enseignement
     * @param str
     * @return 
     */
    public int trouverNiv(String str) {
        int n;
        switch (str) {
            case "Niveau 1":
                n = 1;
                break;
            case "Niveau 2":
                n = 2;
                break;
            case "Niveau 3":
                n = 3;
                break;
            case "Niveau 4":
                n = 4;
                break;
            case "Niveau 5":
                n = 5;
                break;
            case "Niveau 6":
                n = 6;
                break;
            case "Niveau 7":
                n = 7;
                break;
            case "Niveau 8":
                n = 8;
                break;
            case "Niveau 9":
                n = 9;
                break;
            case "Niveau 10":
                n = 10;
                break;
            default:
                n = -1;
                break;
        }
        return n;
    }

    /**
     * intialise les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * initialise les comboBox
     */
    public void initialiserComboBox() {
        // Initialiser le ComboBox Parcours avec tous les parcours
        ComboBoxListeParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, mention.getNom())));
        ComboBoxListeParcours.setValue("Parcours");

        // Initialiser le ComboBox UE avec toutes les UE
        comboBoxListeUE.setItems(FXCollections.observableArrayList(Mention.nomEnseignementPourMention(restaureMention, restaureParcours, mention.getNom())));
        comboBoxListeUE.setValue("UE");

        // Initialiser le ComboBox Code UE
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Mention.codeEnseignementPourMention(restaureMention, restaureParcours, mention.getNom())));
        comboBoxListeCodeUE.setValue("Code UE");

        // Initialiser ComboBoc Niveau (pour le trie)
        ObservableList<String> listeNiv = (ObservableList<String>) FXCollections
                .observableArrayList("Tous les niveaux", "Niveau 1", "Niveau 2", "Niveau 3", "Niveau 4", "Niveau 5", "Niveau 6", "Niveau 7", "Niveau 8", "Niveau 9", "Niveau 10");
        comboBoxNivTrie.setItems(listeNiv);
        comboBoxNivTrie.setValue("Tous les niveaux");
    }

    /**
     * Afficher les enseignements dans le tableau
     * @param liste 
     */
    public void initialiserTableau(ArrayList<Enseignement> liste) {
        tableauEtuParEns.clear();
        if (liste != null) {
            for (Enseignement e : liste) {
                tableauEnseignement.add(e);
            }
            table.setItems(tableauEnseignement);
        }
    }

    /**
     * Récupère les enseignements correspondant à une mention
     */
    public void initialiserListe() {
        listeEnseignement = Parcours.listeEnsMention(restaureParcours, mention.getNom());
        initialiserTableau(listeEnseignement);
    }

    /**
     * Trie les enseignement par rapport à un parcours
     * @param ens
     * @param p
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourParcours(ArrayList<Enseignement> ens, String p) {
        if ((p != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeParcours = new ArrayList<>();
            listeParcours = Parcours.listeEnsParcoursSansMention(restaureParcours, p);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeParcours) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }

    /**
     * Trie les enseignements par rapport à un nom d'enseignement
     * @param ens
     * @param n
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourNom(ArrayList<Enseignement> ens, String n) {
        if ((n != null) && (ens != null)) {
            ens = Enseignement.listeEnsPourNom(ens, n);
            return ens;
        }
        return null;
    }

    /**
     * Trie les enseignements par rapport à un code
     * @param ens
     * @param c
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourCode(ArrayList<Enseignement> ens, String c) {
        if ((c != null) && (ens != null)) {
            ens = Enseignement.listeEnsCode(ens, c);
            return ens;
        }
        return null;
    }

    /**
     * Trie les enseignements par rapport à un code d'enseignement
     * @param ens
     * @param n
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourNiv(ArrayList<Enseignement> ens, int n) {
        if ((ens != null)) {
            ens = Enseignement.listeParNiveauChoisis(ens, n);
            return ens;
        }
        return null;
    }

    /**
     * Afficher les enseignements dans le tableau
     * @param ens 
     */
    public void effectuerTrie(ArrayList<Enseignement> ens) {
        tableauEtuParEns.clear();
        ArrayList<Etudiant> resEtu = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        if (ens != null) {
            for (Enseignement e : ens) {
                EtuParEns ee = new EtuParEns(e);
                ee.setNbEtu(Enseignement.nbEtuSuisUnEns(resEtu, e));
                ee.setNbEtuPrev(Enseignement.nbEtuSuisUnEnsPrevisionnel(resEtu, listeEnseignement, e));
                tableauEtuParEns.add(ee);
            }
        }
        table.setItems(tableauEtuParEns);
    }

    /**
     * Modifie comboBox des noms des enseignements par rapport à un parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxNomP(String p, String nom) {
        comboBoxListeUE.setItems(FXCollections.observableArrayList(Parcours.nomEnseignementPourParcours(restaureParcours, p)));
        comboBoxListeUE.setValue(nom);
    }

    /**
     * Modifie comboBox des codes des enseignements par rapport à un parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxCodeP(String p, String nom) {
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Parcours.codeEnseignementPourParcours(restaureParcours, p)));
        comboBoxListeCodeUE.setValue(nom);
    }

    /**
     * Modifie comboBox des codes des enseignements par rapport à un nom d'enseignement
     * @param n
     * @param nom 
     */
    public void trierComboBoxCodeN(String n, String nom) {
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeStringCodeDonne(restaureEnseignement, n)));
        comboBoxListeCodeUE.setValue(nom);
    }

    /**
     * Modifie comboBox des niveaux des enseignements par rapport à un parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxNiveauP(String p, String nom) {
        ArrayList<Integer> linit = Enseignement.niveauEnseignementParcours(restaureParcours, p);
        ArrayList<String> lfinal = remplirListeEntier(linit);
        comboBoxNivTrie.setItems(FXCollections.observableArrayList(lfinal));
        comboBoxNivTrie.setValue(nom);
    }

    /**
     * Modifie comboBox des niveaux des enseignements par rapport à un nom d'enseignement
     * @param lcode
     * @param nom 
     */
    public void trierComboBoxNiveauN(ArrayList<String> lcode, String nom) {
        ArrayList<Integer> linit = new ArrayList<>();
        for (String c : lcode) {
            linit.add(Enseignement.niveauEnseignementCode(restaureEnseignement, c));
        }
        comboBoxNivTrie.setItems(FXCollections.observableArrayList(remplirListeEntier(linit)));
        comboBoxNivTrie.setValue(nom);
    }

    /**
     * Modifie comboBox des niveaux des enseignements par rapport à un code
     * @param c
     * @param nom 
     */
    public void trierComboBoxNiveauC(String c, String nom) {
        int linit = Enseignement.niveauEnseignementCode(restaureEnseignement, c);
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        tmp.add(linit);
        ArrayList<String> lfinal = remplirListeEntier(tmp);
        comboBoxNivTrie.setItems(FXCollections.observableArrayList(lfinal));
        comboBoxNivTrie.setValue(nom);
    }

    /**
     * Récupérer la liste des niveaux
     * @param li
     * @return 
     */
    public ArrayList<String> remplirListeEntier(ArrayList<Integer> li) {
        ArrayList<String> l = new ArrayList<>();
        for (Integer i : li) {
            switch (i) {
                case 1:
                    if (!l.contains("Niveau 1")) {
                        l.add("Niveau 1");
                    }
                    break;
                case 2:
                    if (!l.contains("Niveau 2")) {
                        l.add("Niveau 2");
                    }
                    break;
                case 3:
                    if (!l.contains("Niveau 3")) {
                        l.add("Niveau 3");
                    }
                    break;
                case 4:
                    if (!l.contains("Niveau 4")) {
                        l.add("Niveau 4");
                    }
                    break;
                case 5:
                    if (!l.contains("Niveau 5")) {
                        l.add("Niveau 5");
                    }
                    break;
                case 6:
                    if (!l.contains("Niveau 6")) {
                        l.add("Niveau 6");
                    }
                    break;
                case 7:
                    if (!l.contains("Niveau 7")) {
                        l.add("Niveau 7");
                    }
                    break;
                case 8:
                    if (!l.contains("Niveau 8")) {
                        l.add("Niveau 8");
                    }
                    break;
                case 9:
                    if (!l.contains("Niveau 9")) {
                        l.add("Niveau 9");
                    }
                    break;
                case 10:
                    if (!l.contains("Niveau 10")) {
                        l.add("Niveau 10");
                    }
                    break;
                default:
                    break;

            }
        }

        return l;
    }

    /**
     * Trier les enseignements en fonction d'un parcours
     */
    public void trieParcours() {
        listeEnseignement = Parcours.listeEnsMention(restaureParcours, mention.getNom());

        // Récupérer valeur dans comboBox
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();
        this.niv = (String) comboBoxNivTrie.getSelectionModel().getSelectedItem();
        if (niv != null) {
            this.n = trouverNiv(niv);
        }

        // Effectuer le trie
        if ((parcours != null) && (ue != null) && (code != null)) {
            if (!parcours.equals("Parcours")) {
                listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                if (!ue.equals("UE")) {
                    listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                    if (!code.equals("Code UE")) {
                        listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {

                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }
                } else {
                    if (!code.equals("Code UE")) {
                        listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {

                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }
                }
                trierComboBoxNomP(parcours, ue);
                trierComboBoxCodeP(parcours, code);
                /*if (n == -1) {
                    trierComboBoxNiveauP(parcours, "Tous les niveaux");
                } else {
                    trierComboBoxNiveauP(parcours, "Niveau " + n);
                }*/

            }
        }
        effectuerTrie(listeEnseignement);
    }

    /**
     * Trier les enseignements en fonction d'un nom d'enseignement
     */
    public void trieNom() {
        listeEnseignement = Parcours.listeEnsMention(restaureParcours, mention.getNom());

        // Récupérer valeur dans comboBox
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();
        this.niv = (String) comboBoxNivTrie.getSelectionModel().getSelectedItem();
        if (niv != null) {
            this.n = trouverNiv(niv);
        }

        // Effectuer le trie
        if ((parcours != null) && (ue != null) && (code != null)) {
            if (!ue.equals("UE")) {
                listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                if (!parcours.equals("Parcours")) {
                    listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                    if (!code.equals("Code UE")) {
                        listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }
                } else {
                    if (!code.equals("Code UE")) {
                        listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }

                }

            }
        }

        effectuerTrie(listeEnseignement);
        trierComboBoxCodeN(ue, code);
        /*ArrayList<String> ar = Enseignement.listeEnsCodeStringCodeDonne(restaureEnseignement, ue);
        if (n == -1) {
            trierComboBoxNiveauN(ar, "Tous les niveaux");
        } else {
            trierComboBoxNiveauN(ar, "Niveau " + n);
        }*/
    }

    /**
     * Trier les enseignements en fonction d'un code d'enseignement
     */
    public void trieCodeUE() {
        listeEnseignement = Parcours.listeEnsMention(restaureParcours, mention.getNom());

        // Récupérer valeur dans comboBox
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();
        this.niv = (String) comboBoxNivTrie.getSelectionModel().getSelectedItem();
        if (niv != null) {
            this.n = trouverNiv(niv);
        }

        // Effectuer le trie
        if ((parcours != null) && (ue != null) && (code != null)) {
            if (!code.equals("Code UE")) {
                listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                if (!parcours.equals("Parcours")) {
                    listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {

                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    } else {

                        if (n != -1) {
                            listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                        }
                    }
                }

            }
        }
        effectuerTrie(listeEnseignement);
        /*if (n == -1) {
            trierComboBoxNiveauC(code, "Tous les niveaux");
        } else {
            trierComboBoxNiveauC(code, "Niveau " + n);
        }*/
    }

    /**
     * Trier les enseignements en fonction d'un niveau
     */
    public void trieNiveau() {
        listeEnseignement = Parcours.listeEnsMention(restaureParcours, mention.getNom());

        // Récupérer valeur dans comboBox
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();
        this.niv = (String) comboBoxNivTrie.getSelectionModel().getSelectedItem();
        if (niv != null) {
            this.n = trouverNiv(niv);
        }
        // Effectuer le trie
        if ((parcours != null) && (ue != null) && (code != null)) {
            if (n != -1) {
                listeEnseignement = enseignementsPourNiv(listeEnseignement, n);
                if (!parcours.equals("Parcours")) {
                    listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                }
            }
        }
        effectuerTrie(listeEnseignement);
    }

    /**
     * afficher le fiche d'une mention
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Enseignement> row = new TableRow<>();
            row.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getClickCount() == 2 && (!row.isEmpty())) {
                        Enseignement enseignement = row.getItem();
                        ArrayList<Etudiant> resEtu = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
                        ArrayList<Enseignement> resEns = Enseignement.restaure(fichierEnseignement);
                        int nbEtud = Enseignement.nbEtuSuisUnEns(resEtu, enseignement);
                        int nbPrev = Enseignement.nbEtuSuisUnEnsPrevisionnel(resEtu, resEns, enseignement);
                        CategoryAxis xAxis = new CategoryAxis();
                        xAxis.setLabel("Année");
                        NumberAxis yAxis = new NumberAxis();
                        yAxis.setLabel("Nombre d'étudiants");
                        BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);
                        barChart.setId("titre");
                        XYChart.Series<String, Number> dataSeries1 = new XYChart.Series<>();
                        dataSeries1.setName(date);
                        dataSeries1.getData().add(new XYChart.Data<>(enseignement.getNom(), nbEtud));
                        XYChart.Series<String, Number> dataSeries2 = new XYChart.Series<>();
                        dataSeries2.setName(datePrev);
                        dataSeries2.getData().add(new XYChart.Data<>(enseignement.getNom(), nbPrev));
                        barChart.getData().add(dataSeries1);
                        barChart.getData().add(dataSeries2);
                        barChart.setTitle("Nombre d'étudiants inscrits à l'UE: " + enseignement.getNom());
                        Label nomUE = new Label(enseignement.getNom());
                        nomUE.setId("labelNomUE");
                        Label mention = new Label("Mention : " + Parcours.mentionPourUE(restaureMention, restaureParcours, enseignement));
                        mention.setId("labelMention");
                        Label parcours = new Label("Parcours : " + Parcours.parcoursPourUE(restaureMention, restaureParcours, enseignement));
                        parcours.setId("labelParcours");
                        Label niveau = new Label("Niveau : " + Enseignement.niveauEnseignement(restaureEnseignement, enseignement));
                        niveau.setId("labelNiveau");
                        Label taux = new Label("Taux de réussite : " + Enseignement.tauxDeReussiteEns(resEtu, restaureEnseignement, enseignement) + "%");
                        taux.setId("labelTaux");

                        VBox vbox = new VBox(nomUE, mention, parcours, niveau, taux, barChart);
                        vbox.setId("vbox");

                        Stage mainStage = new Stage();
                        mainStage.setTitle("Université");
                        Scene scene = new Scene(vbox);
                        scene.getStylesheets().add(ResStatController.this.getClass().getResource("/interfaceGraphique/css/ResStatUE.css").toString());
                        mainStage.setScene(scene);
                        mainStage.setHeight(500);
                        mainStage.setWidth(800);
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setScene(scene);
                        mainStage.setResizable(false);
                        mainStage.show();
                    }
                }
            });
            return row;
        });
    }

    /**
     * Réinitialise les comboBox et la liste 
     */
    public void buttonAnnule() {
        ComboBoxListeParcours.setValue("Parcours");
        comboBoxListeUE.setValue("UE");
        comboBoxListeCodeUE.setValue("Code UE");
        initialiserComboBox();
        initialiserListe();
    }

    /**
     * afficher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * revenir sur la page ResAccueil
     * @throws IOException 
     */
    public void retour() throws IOException {
        Stage source = (Stage) buttonRetour.getScene().getWindow();
        source.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/ResAccueil.fxml"));
        Parent root = loader.load();

        ResAccueilController pageResAccueil = loader.getController();
        pageResAccueil.setRepertoire(repertoire);
        pageResAccueil.setFichierEtudiant(fichierEtudiant);
        pageResAccueil.setFichierEnseignement(fichierEnseignement);
        pageResAccueil.setFichierMention(fichierMention);
        pageResAccueil.setFichierParcours(fichierParcours);
        pageResAccueil.initialiserLesRestaure();
        pageResAccueil.initialiserTableauMention();

        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

}