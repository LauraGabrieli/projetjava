/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class SecFicheEtudiantController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private Etudiant etudiant;

    // Label : données de l'étudiant
    @FXML
    private Label labelPrenomNom;
    @FXML
    private Label labelNumE;
    @FXML
    private Label labelMention;
    @FXML
    private Label labelParcours;

    // Tableau d'UE dont l'étudiant à les pré-requis
    @FXML
    private TableView table;
    @FXML
    private TableColumn columnCode;
    @FXML
    private TableColumn columnNom;

    private ObservableList<Enseignement> tableau = FXCollections.observableArrayList();
    private ArrayList<Enseignement> listeUEPrerequis;
    private String mention;
    private String parcours;

    // ComboBox
    @FXML
    private ComboBox comboBoxParcours;
    @FXML
    private ComboBox comboBoxMention;
    
    // Bouton
    @FXML
    private Button buttonVider;

    /**
     * Initializes the controller class.
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        columnCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));

    }

    /**
     * modifier l'attribut repertoire
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier l'attribut fichierEnseignement
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * initialiser les attributs restaureEtudiant, restaureEnseignement, restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * intialiser les comboBox
     */
    public void initialiserComboBox() {
        // Iniatiliser ComboBox Mention avec toutes les mentions
        comboBoxMention.setItems(FXCollections.observableArrayList(Mention.nomMentions(restaureMention)));
        comboBoxMention.setValue("Mention");

        // Initialiser ComboBox Parcours avec tous les parcours
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");
    }

    /**
     * modifier l'attribut etudiant
     * @param e (Etudiant)
     */
    public void setEtudiant(Etudiant e) {
        etudiant = e;
    }

    /**
     * modifier le label labelPrenomNom
     * @param str (String)
     */
    public void setLabelPrenomNom(String str) {
        labelPrenomNom.setText(str);
    }

    /**
     * modifier le label labelNumE
     * @param num (int)
     */
    public void setLabelNumE(int num) {
        String n = String.valueOf(num);
        labelNumE.setText(n);
    }

    /**
     * modifier le label labelMention
     * @param str (String)
     */
    public void setLabelMention(String str) {
        labelMention.setText(str);
    }

    /**
     * modifier le label labelParcours
     * @param str (String)
     */
    public void setLabelParcours(String str) {
        labelParcours.setText(str);
    }

    /**
     * remplir le tableau avec les enseignements dont l'étudiant a validé les prérequis
     */
    public void listeEUPrerequis() {
        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);
        tableau.clear();

        for (Enseignement e : listeUEPrerequis) {
            tableau.add(e);
        }

        table.setItems(tableau);
    }

    /**
     * filtrer le tableau
     */
    public void tableauEnseignementTriee() {
        listeUEPrerequis = listeUETriee();
        tableau.clear();
        if (listeUEPrerequis != null) {
            for (Enseignement e : listeUEPrerequis) {
                tableau.add(e);
            }
        }

        table.setItems(tableau);
    }

    /**
     * retourne une arrayList d'enseignement en fonction d'une mention ou/et d'un parcours
     * et modifier la valeur des comboBox
     * @return (ArrayList&lt;Enseognement&gt;)
     */
    public ArrayList<Enseignement> listeUETriee() {
        mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();

        ArrayList<Enseignement> listeMention = new ArrayList<>();
        ArrayList<Enseignement> listeParcours = new ArrayList<>();
        ArrayList<Enseignement> listeTemp = new ArrayList<>();

        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);

        tableau.clear();

        if (!mention.equals("Mention") && (listeUEPrerequis != null)) {
            listeMention.clear();
            listeMention = Parcours.listeEnsMention(restaureParcours, mention);
            for (Enseignement e : listeUEPrerequis) {
                for (Enseignement e2 : listeMention) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            listeUEPrerequis.clear();
            listeUEPrerequis.addAll(listeTemp);
            listeTemp.clear();
            if (parcours != null) {
                if (!parcours.equals("Parcours") && (listeUEPrerequis != null)) {
                    listeParcours.clear();
                    listeParcours = Parcours.listeEnsParcours(restaureParcours, parcours);
                    for (Enseignement e : listeUEPrerequis) {
                        for (Enseignement e2 : listeParcours) {
                            if (e.getCode().equals(e2.getCode())) {
                                listeTemp.add(e);
                            }
                        }
                    }
                    listeUEPrerequis.clear();
                    listeUEPrerequis.addAll(listeTemp);
                    listeTemp.clear();
                }
                comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, mention)));
                comboBoxParcours.setValue("Parcours");
            }
        } else {
            if (parcours != null) {
                if (!parcours.equals("Parcours") && (listeUEPrerequis != null)) {
                    listeParcours.clear();
                    listeParcours = Parcours.listeEnsParcours(restaureParcours, parcours);
                    for (Enseignement e : listeUEPrerequis) {
                        for (Enseignement e2 : listeParcours) {
                            if (e.getCode().equals(e2.getCode())) {
                                listeTemp.add(e);
                            }
                        }
                    }
                    listeUEPrerequis.clear();
                    listeUEPrerequis.addAll(listeTemp);
                    listeTemp.clear();
                }
            }
        }
        return listeUEPrerequis;
    }

    /**
     * filtrer le tableau
     */
    public void tableauEnseignementTriee2() {
        listeUEPrerequis = listeUETriee2();
        tableau.clear();
        if (listeUEPrerequis != null) {
            for (Enseignement e : listeUEPrerequis) {
                tableau.add(e);
            }
        }

        table.setItems(tableau);
    }

    /**
     * retourne une arrayList d'enseignement en fonction d'une mention ou/et d'un parcours
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public ArrayList<Enseignement> listeUETriee2() {
        mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();

        ArrayList<Enseignement> listeMention = new ArrayList<>();
        ArrayList<Enseignement> listeParcours = new ArrayList<>();
        ArrayList<Enseignement> listeTemp = new ArrayList<>();

        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);

        tableau.clear();
        if (parcours != null) {
            if (!mention.equals("Mention") && (listeUEPrerequis != null)) {
                listeMention.clear();
                listeMention = Parcours.listeEnsMention(restaureParcours, mention);
                for (Enseignement e : listeUEPrerequis) {
                    for (Enseignement e2 : listeMention) {
                        if (e.getCode().equals(e2.getCode())) {
                            listeTemp.add(e);
                        }
                    }
                }
                listeUEPrerequis.clear();
                listeUEPrerequis.addAll(listeTemp);
                listeTemp.clear();
                if (!parcours.equals("Parcours") && (listeUEPrerequis != null)) {
                    listeParcours.clear();
                    listeParcours = Parcours.listeEnsParcours(restaureParcours, parcours);
                    for (Enseignement e : listeUEPrerequis) {
                        for (Enseignement e2 : listeParcours) {
                            if (e.getCode().equals(e2.getCode())) {
                                listeTemp.add(e);
                            }
                        }
                    }
                    listeUEPrerequis.clear();
                    listeUEPrerequis.addAll(listeTemp);
                    listeTemp.clear();
                }
            } else {
                if (!parcours.equals("Parcours") && (listeUEPrerequis != null)) {
                    listeParcours.clear();
                    listeParcours = Parcours.listeEnsParcours(restaureParcours, parcours);
                    for (Enseignement e : listeUEPrerequis) {
                        for (Enseignement e2 : listeParcours) {
                            if (e.getCode().equals(e2.getCode())) {
                                listeTemp.add(e);
                            }
                        }
                    }
                    listeUEPrerequis.clear();
                    listeUEPrerequis.addAll(listeTemp);
                    listeTemp.clear();
                }
            }
        }
        return listeUEPrerequis;
    }
    
    /**
     * remettre la valeur initiale des comboBox
     */
    public void vider() {
        comboBoxMention.setValue("Mention");
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");

        listeEUPrerequis();
    }

}
