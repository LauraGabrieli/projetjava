/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class PageCreerCompteController implements Initializable {

    // ComboBox
    @FXML
    private ComboBox<String> comboBoxStatus;
    ObservableList<String> comboBoxStatusList = (ObservableList<String>) FXCollections
            .observableArrayList("Directeur d'étude", "Secrétariat pédagogique", "Bureau d'examen", "Responsable de mention");

    // textField
    @FXML
    private TextField textFieldIdentifiant;
    @FXML
    private TextField textFieldMDP;
    @FXML
    private TextField textFieldConfirmationMDP;

    // Bouton
    @FXML
    private Button buttonValider;
    @FXML
    private Button buttonAccueil;
    @FXML
    private Button buttonRetour;

    // label
    @FXML
    private Label labelErreur;
    @FXML
    private Label labelErreurIdentifiant;
    @FXML
    private Label labelMessage;
    @FXML
    private Label labelIdentifiantManquant;
    @FXML
    private Label labelMDPManquant;
    @FXML
    private Label labelConfirmationManquant;

    // Panel
    @FXML
    private AnchorPane panelCompteAjouter;

    private String identifiant;
    private String mdp;
    private String confirmationMdp;
    private String statut;
    private static String fichierIdentifiant;

    /**
     * Initializes the controller class. Initialise le comboBox avec les valeurs
     * directeur d'étude secrétariat pédagogique et bureau d'exament avec pour
     * valeur initiale directeur d'étude
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        comboBoxStatus.setValue("Directeur d'étude");
        comboBoxStatus.setItems(comboBoxStatusList);

        fichierIdentifiant = getPath() + "Données/identifiants.csv";

        labelErreurIdentifiant.setVisible(false);
        panelCompteAjouter.setVisible(false);
        labelErreurIdentifiant.setVisible(false);
        labelIdentifiantManquant.setVisible(false);
        labelMDPManquant.setVisible(false);
        labelConfirmationManquant.setVisible(false);
        labelErreur.setVisible(false);
    }

    /**
     * Récupère le chemin
     *
     * @return
     */
    private static String getPath() {
        String path = PageCreerCompteController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PageCreerCompteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE IDENTIFIANT: "+absolutePath);
        return absolutePath;

    }

    /**
     * Récupère les valeurs des textFiels et du comboBox pour créer un compte
     * Vérifie que le mdp et la confirmation du mdp soient identique Vérifie que
     * l'identifiant n'est déjà pas utilisé pour un statut donné Affiche un
     * message pour informer l'utilisateur de la réussite ou l'échec de la
     * demande
     *
     * @throws IOException
     */
    public void creerCompte() throws IOException {
        identifiant = textFieldIdentifiant.getText();
        mdp = textFieldMDP.getText();
        confirmationMdp = textFieldConfirmationMDP.getText();
        statut = (String) comboBoxStatus.getSelectionModel().getSelectedItem();
        labelErreurIdentifiant.setVisible(false);
        labelIdentifiantManquant.setVisible(false);
        labelMDPManquant.setVisible(false);
        labelConfirmationManquant.setVisible(false);
        labelErreur.setVisible(false);

        if ((identifiant.equals("")) || (mdp.equals("")) || (confirmationMdp.equals(""))) {
            if (identifiant.equals("")) {
                labelIdentifiantManquant.setVisible(true);
            }
            if (mdp.equals("")) {
                labelMDPManquant.setVisible(true);
            }
            if (confirmationMdp.equals("")) {
                labelConfirmationManquant.setVisible(true);
            }
        } else {
            if (!mdp.equals(confirmationMdp)) {
                labelErreur.setVisible(true);
            } else {
                boolean present = false;

                File fichier = new File(fichierIdentifiant);
                if (fichier.exists()) {
                    try {
                        BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(fichierIdentifiant), "UTF-8")); // lis ligne par ligne

                        String l;
                        while ((l = r.readLine()) != null) {
                            String[] ligne = l.split(";");
                            if (ligne[0].equals(identifiant)) { // Déjà dans le fichier
                                labelErreurIdentifiant.setVisible(true);
                                present = true;
                                break;
                            }
                        }
                        if (!present) { // Pas dans le fichier donc on l'ajoute
                            OutputStream out = new FileOutputStream(fichierIdentifiant, true);
                            Writer w = new OutputStreamWriter(out, "UTF-8");
                            w.append(identifiant + ";" + mdp + ";" + statut + "\n");
                            w.close();

                            // On affiche un message pour avertir l'utilisateur que son compte a bien été créé
                            panelCompteAjouter.setVisible(true);
                            labelMessage.setText("Bonjour " + identifiant + "\nVotre compte a bien été créé");

                        }
                        r.close();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                } else {
                    // Si le fichier n'existe pas on ajoute les nouveaux identifiants, mdp et status au fichier
                    try {
                        //FileWriter w = new FileWriter(fichierIdentifiant);
                        OutputStream out = new FileOutputStream(fichierIdentifiant);
                        Writer w = new OutputStreamWriter(out, "UTF-8");
                        w.write('\ufeff');
                        w.append("Identifiant;Mot de passe;Statut\n");
                        w.append(identifiant + ";" + mdp + ";" + statut + "\n");
                        w.close();
                        panelCompteAjouter.setVisible(true);
                        labelMessage.setText("Bonjour " + identifiant + "\nVotre compte a bien été créé");
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }
    }

    /**
     * Permet de revenir sur la page d'accueil
     *
     * @throws IOException
     */
    public void revenirPageAccueil() throws IOException {
        Stage source = (Stage) buttonAccueil.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

}
