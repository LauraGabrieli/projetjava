/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, oceane, carlos
 */
public class DirAccueilController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    // ComboBox
    @FXML
    private ComboBox ComboBoxMention;
    @FXML
    private ComboBox ComboBoxParcours;
    @FXML
    private TableColumn columnNumEtudiant;

    // Tableau
    @FXML
    private TableColumn columnNom;
    @FXML
    private TableColumn columnPrenom;
    @FXML
    private TableColumn columnParcours;
    @FXML
    private TableView table;

    // Butons
    @FXML
    private Button buttonVider;

    private ArrayList<String> listeNomMention;
    private ArrayList<String> listeNomParcours;
    private ArrayList<Etudiant> listeBase;
    private ArrayList<Etudiant> listeEtudiantMention;
    private ArrayList<Etudiant> listeEtudiantParcours;
    private ObservableList<Etudiant> listeEtudiant = FXCollections.observableArrayList();
    private String nomMention;
    private String nomParcours;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;
    @FXML
    private Button buttonFermer;

    /**
     * Initializes the controller class.
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Créer le tableau avec les étudiants
        columnNumEtudiant.setCellValueFactory(new PropertyValueFactory<>("numE"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        columnParcours.setCellValueFactory(new PropertyValueFactory<>("parcours"));

        table.setItems(null);

        tableAction();

        // Cacher panel/label
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);
    }

    /**
     * Modifier l'attribut repertoire
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * Modifier l'attribut fichierEtudiant
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * Modifier l'attribut fichierEnseignement
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * Modifier l'attribut fichierMention
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * initialise les attributs restaureEtudiant, restaureEnseignement, restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);

    }

    /**
     * Initialise les comboBox
     */
    public void initialiserComboBox() {
        // Iniatiliser ComboBox Mention avec toutes les mentions
        ComboBoxMention.setItems(FXCollections.observableArrayList(Mention.nomMentions(restaureMention)));
        ComboBoxMention.setValue("Mention");

        // Initialiser ComboBox Parcours avec tous les parcours
        ComboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        ComboBoxParcours.setValue("Parcours");
    }

    /**
     * remplir le tableau avec tous les étudiants
     */
    public void listeEtudiants() {
        listeBase = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        if (listeBase != null) {
            for (Etudiant e : listeBase) {
                listeEtudiant.add(e);
            }
            table.setItems(listeEtudiant);
        }

    }

    /**
     * Créer une nouvelle liste d'étudiant qui appartiennent à une Mention donnée
     */
    public void listeEtudiantMention() {
        nomMention = (String) ComboBoxMention.getSelectionModel().getSelectedItem();
        nomParcours = (String) ComboBoxParcours.getSelectionModel().getSelectedItem();

        listeEtudiantMention = Etudiant.listeEtuMention(listeBase, nomMention);

        listeEtudiant.clear();

        if(nomParcours!=null){
            if (nomParcours.equals("Parcours")) {
                if (listeEtudiantMention != null) {
                    for (Etudiant e : listeEtudiantMention) {
                        listeEtudiant.add(e);
                    }
                }
                ComboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, nomMention)));
                ComboBoxParcours.setValue("Parcours");
            } else {
                if (listeEtudiantMention != null) {
                    listeEtudiantParcours = Etudiant.listeEtuParcours(listeEtudiantMention, nomParcours);
                    if (listeEtudiantParcours != null) {
                        for (Etudiant e : listeEtudiantParcours) {
                            listeEtudiant.add(e);
                        }
                    }
                }

                ComboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, nomMention)));
                ComboBoxParcours.setValue("Parcours");
            }
        }

        table.setItems(listeEtudiant);
    }

    /**
     * Modifier le tableau en fonction d'un parcours
     */
    public void listeEtudiantParcours() {
        nomMention = (String) ComboBoxMention.getSelectionModel().getSelectedItem();
        nomParcours = (String) ComboBoxParcours.getSelectionModel().getSelectedItem();

        listeEtudiantParcours = Etudiant.listeEtuParcours(listeBase, nomParcours);

        listeEtudiant.clear();

        if (nomMention.equals("Mention")) {
            if (listeEtudiantParcours != null) {
                for (Etudiant e : listeEtudiantParcours) {
                    listeEtudiant.add(e);
                }
            }

        } else {
            if (listeEtudiantParcours != null) {
                listeEtudiantMention = Etudiant.listeEtuParcours(listeEtudiantParcours, nomParcours);
                if (listeEtudiantParcours != null) {
                    for (Etudiant e : listeEtudiantParcours) {
                        listeEtudiant.add(e);
                    }
                }
            }
        }

        table.setItems(listeEtudiant);
    }

    /**
     * Ouvrir la fiche d'un étudiant
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Etudiant> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    try {
                        String prenom = etudiant.getPrenom();
                        String nom = etudiant.getNom();
                        int numE = etudiant.getNumE();
                        String parcours = etudiant.getParcours().getNomP();
                        String mention = etudiant.getParcours().getNom();
                        Stage source = (Stage) table.getScene().getWindow();
                        source.close();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/DirFicheEtudiant.fxml"));
                        Parent root = loader.load();

                        DirFicheEtudiantController pageFicheEtudiant = loader.getController();
                        pageFicheEtudiant.setLabelPrenomNom(prenom + " " + nom);
                        pageFicheEtudiant.setLabelNumE(numE);
                        pageFicheEtudiant.setLabelParcours(parcours);
                        pageFicheEtudiant.setLabelMention(mention);
                        pageFicheEtudiant.setEtudiant(etudiant);
                        pageFicheEtudiant.setRepertoire(repertoire);
                        pageFicheEtudiant.setFichierEtudiant(fichierEtudiant);
                        pageFicheEtudiant.setFichierEnseignement(fichierEnseignement);
                        pageFicheEtudiant.setFichierMention(fichierMention);
                        pageFicheEtudiant.setFichierParcours(fichierParcours);
                        pageFicheEtudiant.initialiserLesRestaure();
                        pageFicheEtudiant.initialiserComboBox();
                        pageFicheEtudiant.listeEUValide();

                        // Ouvre la page PageDemandeDossier
                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            return row;
        });
    }

    /**
     * Afficher le panel avec les boutons "modifier son identifiant", "modifier son mot de passe" et "se déconnecter"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * Cacher le panel avec les boutons "modifier son identifiant", "modifier son mot de passe" et "se déconnecter"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * Se déconnecter
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Remettre les comboBox à leur valeurs initiales
     */
    public void vider() {
        ComboBoxMention.setValue("Mention");
        ComboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        ComboBoxParcours.setValue("Parcours");
        
        listeEtudiants();
    }
}
