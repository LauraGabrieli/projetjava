/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class SecInscriptionController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private ArrayList<Etudiant> listeEtudiantPeutSuivre = new ArrayList<>();
    private ArrayList<Etudiant> listeEtudiantInscrit = new ArrayList<>();
    private ObservableList<Etudiant> tableauEtudiantPeutSuivre = FXCollections.observableArrayList();
    private ObservableList<Etudiant> tableauEtudiantInscrit = FXCollections.observableArrayList();

    private Enseignement enseignement;
    private String numE;
    private String prenom;
    private String nom;

    // Composant du tableau 1
    @FXML
    private TableView table;
    @FXML
    private TableColumn columnNumE;
    @FXML
    private TableColumn columnNom;
    @FXML
    private TableColumn columnPrenom;

    // Composant du tableau Inscrit
    @FXML
    private TableView tableInscrit;
    @FXML
    private TableColumn columnNumEInscrit;
    @FXML
    private TableColumn columnNomInscrit;
    @FXML
    private TableColumn columnPrenomInscrit;

    // Composant pour filtrer
    @FXML
    private TextField textNumE;
    @FXML
    private TextField textPrenom;
    @FXML
    private TextField textNom;

    // Label
    @FXML
    private Label nomUE;

    // label d'erreur
    @FXML
    private Label labelErrNumE;
    @FXML
    private Label labelChampObligatoireUE;
    @FXML
    private Label labelChampObligatoireNumE;

    // Gestion parametres
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    // Message de confirmation
    @FXML
    private AnchorPane panelBloquer;
    @FXML
    private AnchorPane panelConfirmation;
    @FXML
    private Label labelConfirmation;
    @FXML
    private Button buttonConfirmationFermer;

    // button 
    @FXML
    private Button buttonRetour;
    @FXML
    private Button buttonVider;
    @FXML
    private Button buttonSelectTout;
    @FXML
    private Button buttonValider;
    @FXML
    private Button buttonAnnuler;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser les restaure
        restaureEtudiant = new ArrayList<>();
        restaureEnseignement = new ArrayList<>();
        restaureMention = new ArrayList<>();
        restaureParcours = new ArrayList<>();

        // Initialisation du tableau 1
        columnNumE.setCellValueFactory(new PropertyValueFactory<>("numE"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));

        table.setItems(null);

        // Initialisation du tableau 2
        columnNumEInscrit.setCellValueFactory(new PropertyValueFactory<>("numE"));
        columnNomInscrit.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnPrenomInscrit.setCellValueFactory(new PropertyValueFactory<>("prenom"));

        tableInscrit.setItems(null);

        // Autres
        labelErrNumE.setVisible(false);
        panelBloquer.setVisible(false);
        panelConfirmation.setVisible(false);

        tableAction();

        // Cacher 
        panelBloquer.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);
    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * modifier l'attribut enseignement
     *
     * @param ens (Enseignement)
     */
    public void setEnseignement(Enseignement ens) {
        enseignement = ens;
    }

    /**
     * modifier le label correspondant à l'enseignement
     *
     * @param str (String)
     */
    public void labelEnseignement(String str) {
        nomUE.setText(str);
    }

    /**
     * intialise les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * Afficher dans un tableau les étudiants pourvant suivre une UE donnée
     */
    public void afficherListeEtudiantPeutSuivre() {
        tableauEtudiantPeutSuivre.clear();
        listeEtudiantPeutSuivre = listeEtudiantPeutSuivre();
        for (Etudiant e : listeEtudiantPeutSuivre) {
            tableauEtudiantPeutSuivre.add(e);
        }
        table.setItems(tableauEtudiantPeutSuivre);
    }

    /**
     * liste de tous les étudiants pouvant s'inscrire à l'enseignement
     *
     * @return
     */
    public ArrayList<Etudiant> listeEtudiantPeutSuivre() {
        ArrayList<Etudiant> listeEtu = new ArrayList<>();
        ArrayList<Etudiant> listeEtu2 = new ArrayList<>();
        for (Etudiant e : Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant)) {
            for (Enseignement ens : e.listeEnsPossible(restaureEnseignement)) {
                if ((ens.getCode().equals(enseignement.getCode())) && !listeEtu.contains(e)) {
                    listeEtu.add(e);
                }
            }
        }

        if (!listeEtudiantInscrit.isEmpty()) {
            for (Etudiant e : listeEtu) {
                boolean present = false;
                for (Etudiant e2 : listeEtudiantInscrit) {
                    int etu1 = e.getNumE();
                    int etu2 = e2.getNumE();
                    System.out.println("e1 : " + etu1 + " , e2 : " + etu2);
                    if ((etu1 == etu2)) {
                        present = true;

                    }
                }
                if (!present) {
                    listeEtu2.add(e);
                }
            }
            return listeEtu2;
        }
        return listeEtu;
    }

    /**
     * Afficher les étudiants dans le tableau
     * @param etu 
     */
    public void effectuerTrie(ArrayList<Etudiant> etu) {
        tableauEtudiantPeutSuivre.clear();
        if (etu != null) {
            for (Etudiant e : etu) {
                tableauEtudiantPeutSuivre.add(e);
            }
        }
        table.setItems(tableauEtudiantPeutSuivre);
    }

    /**
     * Filtre les étudiants
     */
    public void filtrer() {
        labelErrNumE.setVisible(false);

        numE = textNumE.getText();
        prenom = textPrenom.getText();
        nom = textNom.getText();

        listeEtudiantPeutSuivre.clear();
        listeEtudiantPeutSuivre = listeEtudiantPeutSuivre();

        if (!numE.equals("")) {
            try {
                int num = Integer.parseInt(numE);
                listeEtudiantPeutSuivre = Etudiant.listeEtuNumE(listeEtudiantPeutSuivre, num);
                if (!prenom.equals("") && (listeEtudiantPeutSuivre != null)) {
                    listeEtudiantPeutSuivre = Etudiant.listeEtuPrenom(listeEtudiantPeutSuivre, prenom);
                    if (!nom.equals("") && (listeEtudiantPeutSuivre != null)) {
                        listeEtudiantPeutSuivre = Etudiant.listeEtuNom(listeEtudiantPeutSuivre, nom);
                    }
                } else {
                    if (!nom.equals("") && (listeEtudiantPeutSuivre != null)) {
                        listeEtudiantPeutSuivre = Etudiant.listeEtuNom(listeEtudiantPeutSuivre, nom);
                    }
                }
            } catch (NumberFormatException e) {
                labelErrNumE.setVisible(true);
            }
        } else {
            if (!prenom.equals("") && (listeEtudiantPeutSuivre != null)) {
                listeEtudiantPeutSuivre = Etudiant.listeEtuPrenom(listeEtudiantPeutSuivre, prenom);
                if (!nom.equals("") && (listeEtudiantPeutSuivre != null)) {
                    listeEtudiantPeutSuivre = Etudiant.listeEtuNom(listeEtudiantPeutSuivre, nom);
                }
            } else {
                if (!nom.equals("") && (listeEtudiantPeutSuivre != null)) {
                    listeEtudiantPeutSuivre = Etudiant.listeEtuNom(listeEtudiantPeutSuivre, nom);
                }
            }
        }
        effectuerTrie(listeEtudiantPeutSuivre);
    }

    /**
     * Inscrire un étudiant à une mention
     * @param e 
     */
    public void tableau1a2(Etudiant e) {
        listeEtudiantInscrit.add(e);
        tableauEtudiantInscrit.add(e);

        listeEtudiantPeutSuivre.remove(e);
        tableauEtudiantPeutSuivre.remove(e);

        table.setItems(tableauEtudiantPeutSuivre);
        tableInscrit.setItems(tableauEtudiantInscrit);
    }

    /**
     * Annuler l'inscription
     * @param e 
     */
    public void tableau2a1(Etudiant e) {
        listeEtudiantInscrit.remove(e);
        tableauEtudiantInscrit.remove(e);

        listeEtudiantPeutSuivre.add(e);
        tableauEtudiantPeutSuivre.add(e);

        table.setItems(tableauEtudiantPeutSuivre);
        tableInscrit.setItems(tableauEtudiantInscrit);
    }

    /**
     * afficher le fiche d'un étudiant
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Etudiant> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/SecFicheEtudiant.fxml"));
                        Parent root = loader.load();

                        // Permet de sauvegarde les différents éléments d'un étudiant
                        SecFicheEtudiantController pageSecFicheEtudiant = loader.getController();
                        pageSecFicheEtudiant.setLabelPrenomNom(etudiant.getPrenom() + " " + etudiant.getNom());
                        pageSecFicheEtudiant.setLabelNumE(etudiant.getNumE());
                        pageSecFicheEtudiant.setLabelParcours(etudiant.getParcours().toString());
                        pageSecFicheEtudiant.setLabelMention(etudiant.getParcours().getNom());
                        pageSecFicheEtudiant.setRepertoire(repertoire);
                        pageSecFicheEtudiant.setFichierEtudiant(fichierEtudiant);
                        pageSecFicheEtudiant.setFichierEnseignement(fichierEnseignement);
                        pageSecFicheEtudiant.setFichierMention(fichierMention);
                        pageSecFicheEtudiant.setFichierParcours(fichierParcours);
                        pageSecFicheEtudiant.setEtudiant(etudiant);
                        pageSecFicheEtudiant.initialiserLesRestaure();
                        pageSecFicheEtudiant.initialiserComboBox();
                        pageSecFicheEtudiant.listeEUPrerequis();

                        // Ouvre la page PageDemandeDossier
                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    tableau1a2(etudiant);
                }
            });
            return row;
        });

        tableInscrit.setRowFactory(tv -> {
            TableRow<Etudiant> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/SecFicheEtudiant.fxml"));
                        Parent root = loader.load();

                        // Permet de sauvegarde les différents éléments d'un étudiant
                        SecFicheEtudiantController pageSecFicheEtudiant = loader.getController();
                        pageSecFicheEtudiant.setLabelPrenomNom(etudiant.getPrenom() + " " + etudiant.getNom());
                        pageSecFicheEtudiant.setLabelNumE(etudiant.getNumE());
                        pageSecFicheEtudiant.setLabelParcours(etudiant.getParcours().toString());
                        pageSecFicheEtudiant.setLabelMention(etudiant.getParcours().getNom());
                        pageSecFicheEtudiant.setRepertoire(repertoire);
                        pageSecFicheEtudiant.setFichierEtudiant(fichierEtudiant);
                        pageSecFicheEtudiant.setFichierEnseignement(fichierEnseignement);
                        pageSecFicheEtudiant.setFichierMention(fichierMention);
                        pageSecFicheEtudiant.setFichierParcours(fichierParcours);
                        pageSecFicheEtudiant.setEtudiant(etudiant);
                        pageSecFicheEtudiant.initialiserLesRestaure();
                        pageSecFicheEtudiant.initialiserComboBox();
                        pageSecFicheEtudiant.listeEUPrerequis();

                        // Ouvre la page PageDemandeDossier
                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    tableau2a1(etudiant);
                }
            });
            return row;
        });
    }

    /**
     * ajouter un enseignement à un étudiant
     *
     * @throws InterruptedException
     */
    public void buttonAjouter() throws InterruptedException {
        if (!listeEtudiantInscrit.isEmpty()) {
            for (Etudiant e : listeEtudiantInscrit) {
                e.ajouterEnseignement(Enseignement.listeEnsCodeBis(restaureEnseignement, enseignement.getCode()));
                e.sauvegarde();
            }
            listeEtudiantInscrit.clear();
            tableauEtudiantInscrit.clear();

            panelBloquer.setVisible(true);
            panelConfirmation.setVisible(true);
        }

    }

    public void fermerPanelConfirmation() {
        panelBloquer.setVisible(false);
        panelConfirmation.setVisible(false);
    }

    /**
     * afficher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelBloquer.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelBloquer.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * retourner sur la page secAccueil
     * @throws IOException 
     */
    public void retour() throws IOException {
        Stage source = (Stage) buttonRetour.getScene().getWindow();
        source.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/SecAccueil.fxml"));
        Parent root = loader.load();

        SecAccueilController pageSecAccueil = loader.getController();
        pageSecAccueil.setRepertoire(repertoire);
        pageSecAccueil.setFichierEtudiant(fichierEtudiant);
        pageSecAccueil.setFichierEnseignement(fichierEnseignement);
        pageSecAccueil.setFichierMention(fichierMention);
        pageSecAccueil.setFichierParcours(fichierParcours);
        pageSecAccueil.initialiserLesRestaure();
        pageSecAccueil.initialiserComboBox();
        pageSecAccueil.initialiserTableauUE();

        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * vider les textFields
     * Réinitialiser la liste des étudiants
     */
    public void vider() {
        textNumE.clear();
        textPrenom.clear();
        textNom.clear();
        afficherListeEtudiantPeutSuivre();
    }

    /**
     * Sélectionner tous les étudiants
     */
    public void selectionnerTout() {
        if (listeEtudiantPeutSuivre != null) {
            for (Etudiant e : listeEtudiantPeutSuivre) {
                listeEtudiantInscrit.add(e);
                tableauEtudiantInscrit.add(e);
            }
            listeEtudiantPeutSuivre.clear();
            tableauEtudiantPeutSuivre.clear();
        }
        table.setItems(tableauEtudiantPeutSuivre);
        tableInscrit.setItems(tableauEtudiantInscrit);
    }

    /**
     * Annuler toutes les modifications (inscriptions)
     */
    public void annuler() {
        if (!listeEtudiantInscrit.isEmpty()) {
            ArrayList<Etudiant> temp = new ArrayList<>();
            temp.addAll(listeEtudiantInscrit);

            for (Etudiant e : temp) {
                tableau2a1(e);
            }
            listeEtudiantInscrit.clear();
            tableauEtudiantInscrit.clear();
        }
        table.setItems(tableauEtudiantPeutSuivre);
        tableInscrit.setItems(tableauEtudiantInscrit);
    }
}
