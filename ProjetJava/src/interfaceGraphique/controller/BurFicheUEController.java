/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import gestion.Suivi;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class BurFicheUEController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private Enseignement enseignement;
    //private ArrayList<Etudiant> listeEtudiant = new ArrayList<>();
    private ArrayList<Etudiant> listeEtudiantUENonValide = new ArrayList<>();
    private ArrayList<Etudiant> listeEtudiantUEValide = new ArrayList<>();
    private ObservableList<Etudiant> tableauEtudNonVal = FXCollections.observableArrayList();
    private ObservableList<Etudiant> tableauEtudVal = FXCollections.observableArrayList();

    // Label
    @FXML
    private Label labelUECodeNom;
    @FXML
    private Label labelListeVide;
    @FXML
    private Label labelErrNumE;

    // Boutons
    @FXML
    private Button buttonSauvegarde;
    @FXML
    private Button buttonAnnuler;
    @FXML
    private Button buttonFermer;

    // Tableau Etudiant qui n'ont pas validé l'UE
    @FXML
    private TableView tableNonValide;
    @FXML
    private TableColumn columnNonValNumE;
    @FXML
    private TableColumn columnNonValNom;
    @FXML
    private TableColumn columnNonValPrenom;

    // Tableau Etudiant qui ont validé UE
    @FXML
    private TableView tableValide;
    @FXML
    private TableColumn columnValNumE;
    @FXML
    private TableColumn columnValNom;
    @FXML
    private TableColumn columnValPrenom;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;
    @FXML
    private Button buttonRetour;

    // Confirmation
    @FXML
    private AnchorPane panelCacherConfirmation;
    @FXML
    private AnchorPane panelConfirmation;
    @FXML
    private Button buttonConfirmationFermer;

    // Composant pour filtrer
    @FXML
    private TextField textNumE;
    @FXML
    private TextField textPrenom;
    @FXML
    private TextField textNom;

    // Variable pour trie
    private String numE;
    private String prenom;
    private String nom;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableAction();

        // Tableau Etudiant qui n'ont pas validé l'UE        
        columnNonValNumE.setCellValueFactory(new PropertyValueFactory<>("numE"));
        columnNonValNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnNonValPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        tableNonValide.setItems(null);

        // Tableau Etudiant qui n'ont pas validé l'UE        
        columnValNumE.setCellValueFactory(new PropertyValueFactory<>("numE"));
        columnValNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        columnValPrenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        tableValide.setItems(null);

        // Cacher panel/label
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);
        panelCacherConfirmation.setVisible(false);
        panelConfirmation.setVisible(false);
    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier l'attribut fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * initialise les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * modifier l'attribut enseignement
     *
     * @param e (Enseignement)
     */
    public void setUE(Enseignement e) {
        enseignement = e;
    }

    /**
     * remplir le tableau avec les étudiants qui n'ont pas validés
     * l'enseignement
     */
    public void initialiserTableau() {
        ArrayList<Etudiant> liste = listeEtudiant();

        if ( liste != null ) {
            tableauEtudNonVal.clear();
            for (Etudiant e : liste) {
                if (!e.aValideEns(enseignement)) {
                    tableauEtudNonVal.add(e);
                    listeEtudiantUENonValide.add(e);
                }
            }
            tableNonValide.setItems(tableauEtudNonVal);
        }
    }

    /**
     * modifier les tableaux validés et non validés valider un enseignement pour
     * un étudiant
     *
     * @param e (Etudiant)
     */
    public void tableauNonValAVal(Etudiant e) {
        tableauEtudVal.add(e);
        listeEtudiantUEValide.add(e);

        tableauEtudNonVal.remove(e);
        listeEtudiantUENonValide.remove(e);

        tableValide.setItems(tableauEtudVal);
        tableNonValide.setItems(tableauEtudNonVal);
    }

    /**
     * modifier les tableaux validés et non validés et enlever la validation
     * d'un enseignement pour un étudiant
     *
     * @param e (Etudiant)
     */
    public void tableauValANonVal(Etudiant e) {
        if (e != null) {
            tableauEtudVal.remove(e);
            listeEtudiantUEValide.remove(e);

            tableauEtudNonVal.add(e);
            listeEtudiantUENonValide.add(e);

            tableValide.setItems(tableauEtudVal);
            tableNonValide.setItems(tableauEtudNonVal);
        }
    }

    /**
     * retourne la liste des étudiants qui n'ont pas encore validé l'UE
     *
     * @return
     */
    public ArrayList<Etudiant> listeEtudiant() {
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        ArrayList<Etudiant> listeEtudiant = Etudiant.listeEtudE(restaureEtudiant, enseignement);
        ArrayList<Etudiant> listeEtudiant2 = new ArrayList<>();

        if (!listeEtudiantUEValide.isEmpty()) {
            for (Etudiant e : listeEtudiant) {
                boolean present = false;
                for (Etudiant e2 : listeEtudiantUEValide) {
                    int etu1 = e.getNumE();
                    int etu2 = e2.getNumE();
                    if ((etu1 == etu2)) {
                        present = true;
                    }
                }
                if (!present) {
                    listeEtudiant2.add(e);
                }
            }
            System.out.println("test2 : " + listeEtudiant2.size());
            return listeEtudiant2;
        }
        System.out.println("test3 : " + listeEtudiant.size());
        return listeEtudiant;
    }

    /**
     * modifier l'attribut enseignement
     *
     * @param e (Enseignement)
     */
    public void setEnseignement(Enseignement e) {
        enseignement = e;
    }

    /**
     * modifier le label labelUECodeNom
     *
     * @param str (String)
     */
    public void setLabelUECodeNom(String str) {
        labelUECodeNom.setText(str);
    }

    /**
     * sélectionner les étudiants qui ont validés l'enseignement
     */
    public void tableAction() {
        tableNonValide.setRowFactory(tv -> {
            TableRow<Etudiant> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    if (!etudiant.aValideEns(enseignement)) {
                        for (Suivi s : etudiant.getListeSuivi()) {
                            if (s.getEnseignement().getCode().equals(enseignement.getCode())) {
                                s.setValide(true);
                            }
                        }
                        tableauNonValAVal(etudiant);
                    }
                }
            });
            return row;
        });

        tableValide.setRowFactory(tv -> {
            TableRow<Etudiant> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    Etudiant etudiant = row.getItem();
                    for (Suivi s : etudiant.getListeSuivi()) {
                        if (s.getEnseignement().getCode().equals(enseignement.getCode())) {
                            s.setValide(false);
                        }
                    }
                    tableauValANonVal(etudiant);

                }
            });
            return row;
        });
    }

    /**
     * sauvegarder les modifications et validation d'un enseignement
     */
    public void sauvegarder() {
        if (!tableauEtudVal.isEmpty()) {
            for (Etudiant e : tableauEtudVal) {
                e.sauvegarde();
            }
            tableauEtudVal.clear();
            panelCacherConfirmation.setVisible(true);
            panelConfirmation.setVisible(true);
        }

    }

    /**
     * Afficher message de confirmation
     */
    public void fermerPanelConfirmation() {
        panelCacherConfirmation.setVisible(false);
        panelConfirmation.setVisible(false);
    }

    /**
     * annuler toutes les modifications faites (validation d'un enseignement
     */
    public void annuler() {
        if (!listeEtudiantUEValide.isEmpty()) {
            ArrayList<Etudiant> etu = new ArrayList<>();
            etu.addAll(listeEtudiantUEValide);

            tableauEtudVal.clear();

            for (Etudiant e : etu) {
                for (Suivi s : e.getListeSuivi()) {
                    if (s.getEnseignement().getCode().equals(enseignement.getCode())) {
                        s.setValide(false);
                    }
                }
                tableauValANonVal(e);
            }
            listeEtudiantUEValide.clear();
            tableauEtudVal.clear();
        }
        tableNonValide.setItems(tableauEtudNonVal);
        tableValide.setItems(tableauEtudVal);
    }

    /**
     * afficher les étudiants par rapport à une arraylist
     *
     * @param etu
     */
    public void effectuerTrie(ArrayList<Etudiant> etu) {
        tableauEtudNonVal.clear();
        if (etu != null) {
            for (Etudiant e : etu) {
                tableauEtudNonVal.add(e);
            }
        }
        tableNonValide.setItems(tableauEtudNonVal);
    }

    /**
     * Filtrer les étudiants
     */
    public void filtrer() {
        labelErrNumE.setVisible(false);
        numE = textNumE.getText();
        prenom = textPrenom.getText();
        nom = textNom.getText();

        if (listeEtudiantUENonValide != null) {
            listeEtudiantUENonValide.clear();
        }
        listeEtudiantUENonValide = listeEtudiant();

        if (!numE.equals("")) {
            try {
                int num = Integer.parseInt(numE);
                listeEtudiantUENonValide = Etudiant.listeEtuNumE(listeEtudiantUENonValide, num);
                if (!prenom.equals("") && (listeEtudiantUENonValide != null)) {
                    listeEtudiantUENonValide = Etudiant.listeEtuPrenom(listeEtudiantUENonValide, prenom);
                    if (!nom.equals("") && (listeEtudiantUENonValide != null)) {
                        listeEtudiantUENonValide = Etudiant.listeEtuNom(listeEtudiantUENonValide, nom);
                    }
                } else {
                    if (!nom.equals("") && (listeEtudiantUENonValide != null)) {
                        listeEtudiantUENonValide = Etudiant.listeEtuNom(listeEtudiantUENonValide, nom);
                    }
                }
            } catch (NumberFormatException e) {
                labelErrNumE.setVisible(true);
            }
        } else {
            if (!prenom.equals("") && (listeEtudiantUENonValide != null)) {
                listeEtudiantUENonValide = Etudiant.listeEtuPrenom(listeEtudiantUENonValide, prenom);
                if (!nom.equals("") && (listeEtudiantUENonValide != null)) {
                    listeEtudiantUENonValide = Etudiant.listeEtuNom(listeEtudiantUENonValide, nom);
                }
            } else {
                if (!nom.equals("") && (listeEtudiantUENonValide != null)) {
                    listeEtudiantUENonValide = Etudiant.listeEtuNom(listeEtudiantUENonValide, nom);
                }
            }
        }
        effectuerTrie(listeEtudiantUENonValide);
    }

    /**
     * afficher les boutons "se déconnecter", "modifier son identifaint" et
     * "modifier son mot de pass"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son identifaint" et
     * "modifier son mot de pass"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * revenir à la page BurAccueilController
     *
     * @throws IOException
     */
    public void retour() throws IOException {
        Stage source = (Stage) buttonRetour.getScene().getWindow();
        source.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/BurAccueil.fxml"));
        Parent root = loader.load();

        BurAccueilController pageBurAccueil = loader.getController();
        pageBurAccueil.setRepertoire(repertoire);
        pageBurAccueil.setFichierEtudiant(fichierEtudiant);
        pageBurAccueil.setFichierEnseignement(fichierEnseignement);
        pageBurAccueil.setFichierMention(fichierMention);
        pageBurAccueil.setFichierParcours(fichierParcours);
        pageBurAccueil.initialiserLesRestaure();
        pageBurAccueil.initialiserComboBox();
        pageBurAccueil.initialiserTableauUE();

        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Vide les comboBox et les textFiels Réinitialise la liste des étudiants
     * dans le tableau
     */
    public void vider() {
        textNumE.clear();
        textPrenom.clear();
        textNom.clear();
        initialiserTableau();
    }

    /**
     * Sélectionne tous les étudiants
     */
    public void selectionnerTout() {
        if (listeEtudiantUENonValide != null) {
            System.out.println("test4 : " + listeEtudiantUENonValide.size());
            for (Etudiant e : listeEtudiantUENonValide) {
                listeEtudiantUEValide.add(e);
                tableauEtudVal.add(e);
            }
            listeEtudiantUENonValide.clear();
            tableauEtudNonVal.clear();
        }
        tableNonValide.setItems(tableauEtudNonVal);
        tableValide.setItems(tableauEtudVal);
    }
}
