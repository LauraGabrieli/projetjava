/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura
 */
public class DirFicheEtudiantController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private String parcours;
    private String mention;
    private Etudiant etudiant;
    private ArrayList<Enseignement> listeUEValide;
    private ArrayList<Enseignement> listeUENonValide;
    private ArrayList<Enseignement> listeUEEnCours;
    private ArrayList<Enseignement> listeUEPrerequis;
    private ArrayList<Enseignement> listeUEPrerequisMention;
    private ArrayList<Enseignement> listeUEPrerequisParcours;
    private ObservableList<Enseignement> tableauValide = FXCollections.observableArrayList();
    private ObservableList<Enseignement> tableauNonValide = FXCollections.observableArrayList();
    private ObservableList<Enseignement> tableauEnCours = FXCollections.observableArrayList();
    private ObservableList<Enseignement> tableauPrerequis = FXCollections.observableArrayList();
    private ObservableList<Enseignement> tableauUEOuverture = FXCollections.observableArrayList();

    private ArrayList<Enseignement> listeEnseignementMention;
    private ArrayList<Enseignement> listeEnseignementParcours;

    private ArrayList<Mention> listeMention;
    private ArrayList<Parcours> listeParcours;

    // Label
    @FXML
    private Label LabelPrenomNom;
    @FXML
    private Label labelNumE;
    @FXML
    private Label labelParcours;
    @FXML
    private Label labelMention;

    // Tableau UE validés
    @FXML
    private AnchorPane panelUEValide;
    @FXML
    private TableView tableValide;
    @FXML
    private TableColumn columnValideUE;
    @FXML
    private TableColumn columnCodeValide;
    @FXML
    private Button buttonUEValide;

    // Tableau UE non validés
    @FXML
    private TableView tableNonValide;
    @FXML
    private TableColumn columnNonValideUE;
    @FXML
    private TableColumn columnCodeNonValide;

    // Tableau UE en cours
    @FXML
    private AnchorPane panelUEEnCours;
    @FXML
    private TableView tableUEEnCours;
    @FXML
    private TableColumn columnCodeEnCours;
    @FXML
    private TableColumn columnUEEnCours;
    @FXML
    private Button buttonUEEnCours;

    // UE dont l'étudiant à les prérequis
    @FXML
    private AnchorPane panelUEPrerequis;
    @FXML
    private Button buttonUEPrerequis;
    @FXML
    private TableColumn columnCodePrerequis;
    @FXML
    private TableColumn columnUEPrerequis;
    @FXML
    private TableView tablePrerequis;

    // ComboBox
    @FXML
    private ComboBox comboBoxMention;
    @FXML
    private ComboBox comboBoxParcours;

    // UE d'ouverture
    @FXML
    private TableView tableOuverture;
    @FXML
    private TableColumn columnCodeUEOuverture;
    @FXML
    private TableColumn columnUEOuverture;
    @FXML
    private Button buttonRetour;

    // Gestion des paramètres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    // Panel
    @FXML
    private AnchorPane panelboutonUEValider;
    @FXML
    private AnchorPane panelboutonUEEnCours;
    @FXML
    private AnchorPane panelboutonUEDispo;

    // Bouton
    @FXML
    private Button vider;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser les restaure
        restaureEtudiant = new ArrayList<>();
        restaureEnseignement = new ArrayList<>();
        restaureMention = new ArrayList<>();
        restaureParcours = new ArrayList<>();

        /* AnchorPane pour les UE validés ou non validés + initialisation tableau */
        panelUEValide.setVisible(true);
        panelboutonUEValider.setVisible(true);

        columnCodeValide.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnValideUE.setCellValueFactory(new PropertyValueFactory<>("nom"));

        columnCodeNonValide.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnNonValideUE.setCellValueFactory(new PropertyValueFactory<>("nom"));

        tableValide.setItems(null);
        tableNonValide.setItems(null);

        /* AnchorPane pour les UE en cours + initialisation tableau */
        panelUEEnCours.setVisible(false);
        panelboutonUEEnCours.setVisible(false);

        columnCodeEnCours.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnUEEnCours.setCellValueFactory(new PropertyValueFactory<>("nom"));

        tableUEEnCours.setItems(null);

        /* AnchorPane pour les UE dont l'étudiant à les prérequis + UE d'ouverture */
        /* UE dont l'étudiant à les prérequis + initialisation tableau */
        panelUEPrerequis.setVisible(false);
        panelboutonUEDispo.setVisible(false);

        columnCodePrerequis.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnUEPrerequis.setCellValueFactory(new PropertyValueFactory<>("nom"));

        tablePrerequis.setItems(null);

        /* UE d'ouverture + initialisation tableau */
        columnCodeUEOuverture.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnUEOuverture.setCellValueFactory(new PropertyValueFactory<>("nom"));

        tableOuverture.setItems(null);

        // Cacher panel/label
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);
    }

    /**
     * modifier le label LabelPrenomNom
     *
     * @param str (String)
     */
    public void setLabelPrenomNom(String str) {
        LabelPrenomNom.setText(str);
    }

    /**
     * modifier le label labelNumE
     *
     * @param num (int)
     */
    public void setLabelNumE(int num) {
        labelNumE.setText(Integer.toString(num));
    }

    /**
     * modifier le label labelParcours
     *
     * @param str (String)
     */
    public void setLabelParcours(String str) {
        labelParcours.setText(str);
    }

    /**
     * modifier le label labelMention
     *
     * @param str (String)
     */
    public void setLabelMention(String str) {
        labelMention.setText(str);
    }

    /**
     * modifier l'attribut etudiant
     *
     * @param e (Etudiant)
     */
    public void setEtudiant(Etudiant e) {
        etudiant = e;
    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier l'attribut fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * initialiser les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * initialiser les comboBox
     */
    public void initialiserComboBox() {
        // Iniatiliser ComboBox Mention avec toutes les mentions
        comboBoxMention.setItems(FXCollections.observableArrayList(Mention.nomMentions(restaureMention)));
        comboBoxMention.setValue("Mention");

        // Initialiser ComboBox Parcours avec tous les parcours
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");
    }

    /**
     * remplir le tableau des enseignements validés et le tableau des
     * enseignements non validés
     */
    public void listeEUValide() {
        panelUEValide.setVisible(true);
        panelboutonUEValider.setVisible(true);
        panelUEEnCours.setVisible(false);
        panelboutonUEEnCours.setVisible(false);
        panelUEPrerequis.setVisible(false);
        panelboutonUEDispo.setVisible(false);

        listeUEValide = etudiant.ueValides();
        listeUENonValide = etudiant.ueNonValides();

        tableauValide.clear();
        tableauNonValide.clear();

        for (Enseignement e : listeUEValide) {
            tableauValide.add(e);
        }
        for (Enseignement e : listeUENonValide) {
            tableauNonValide.add(e);
        }

        tableValide.setItems(tableauValide);
        tableNonValide.setItems(tableauNonValide);
    }

    /**
     * remplir le tableau des enseignements en cours
     */
    public void listeEUEnCours() {
        panelUEValide.setVisible(false);
        panelboutonUEValider.setVisible(false);
        panelUEEnCours.setVisible(true);
        panelboutonUEEnCours.setVisible(true);
        panelUEPrerequis.setVisible(false);
        panelboutonUEDispo.setVisible(false);

        listeUEEnCours = etudiant.ueEnCours();
        tableauEnCours.clear();

        for (Enseignement e : listeUEEnCours) {
            tableauEnCours.add(e);
        }

        tableUEEnCours.setItems(tableauEnCours);
    }

    /**
     * remplir le tableau avec les enseignements dont on a validés les prérequis
     */
    public void listeEUPrerequis() {
        panelUEValide.setVisible(false);
        panelboutonUEValider.setVisible(false);
        panelUEEnCours.setVisible(false);
        panelboutonUEEnCours.setVisible(false);
        panelUEPrerequis.setVisible(true);
        panelboutonUEDispo.setVisible(true);

        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);
        tableauPrerequis.clear();
        tableauUEOuverture.clear();

        for (Enseignement e : listeUEPrerequis) {
            if (e.isDecouverte()) {
                tableauUEOuverture.add(e);
            } else {
                tableauPrerequis.add(e);
            }

        }

        tablePrerequis.setItems(tableauPrerequis);
        tableOuverture.setItems(tableauUEOuverture);
    }

    /**
     * modifier le tableau des enseignements dont on a les prérequis en fonction
     * d'une mention et d'un parcours en modifiant les valeurs du comboBox
     * comboBoxParcours si nécessaire
     */
    public void listeEnsMentionParcours() {
        mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();

        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);

        if (listeUEPrerequis != null) {
            if (!mention.equals("Mention")) {

                if (listeUEPrerequis != null) {
                    listeUEPrerequis = etudiant.listeEnsPossibleMention(listeUEPrerequis, restaureMention, restaureParcours, mention);
                    if (!parcours.equals("Parcours") && listeUEPrerequis != null) {
                        listeUEPrerequis = etudiant.listeEnsPossibleParcours(listeUEPrerequis, restaureParcours, parcours);
                    }

                }
                comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, mention)));
                comboBoxParcours.setValue("Parcours");
            } else {
                if (!parcours.equals("Parcours")) {
                    listeUEPrerequis = etudiant.listeEnsPossibleParcours(listeUEPrerequis, restaureParcours, parcours);
                }
            }
            tableauPrerequis.clear();
            if (listeUEPrerequis != null) {
                for (Enseignement e : listeUEPrerequis) {
                    tableauPrerequis.add(e);
                }
            }
        }

        tablePrerequis.setItems(tableauPrerequis);

    }

    /**
     * modifier le tableau des enseignements dont on a les prérequis en fonction
     * d'une mention et d'un parcours
     */
    public void listeEnsMentionParcours2() {
        mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();

        listeUEPrerequis = etudiant.listeEnsPossible(restaureEnseignement);

        if (!mention.equals("Mention")) {
            if (listeUEPrerequis != null) {
                listeUEPrerequis = etudiant.listeEnsPossibleMention(listeUEPrerequis, restaureMention, restaureParcours, mention);
                if (parcours != null) {
                    if (!parcours.equals("Parcours") && listeUEPrerequis != null) {
                        listeUEPrerequis = etudiant.listeEnsPossibleParcours(listeUEPrerequis, restaureParcours, parcours);
                    }
                }
            }
        } else {
            if (listeUEPrerequis != null) {
                if (parcours != null) {
                    if (!parcours.equals("Parcours")) {
                        listeUEPrerequis = etudiant.listeEnsPossibleParcours(listeUEPrerequis, restaureParcours, parcours);
                    }
                }
            }
        }
        tableauPrerequis.clear();

        if (listeUEPrerequis != null) {
            for (Enseignement e : listeUEPrerequis) {
                tableauPrerequis.add(e);
            }
        }

        tablePrerequis.setItems(tableauPrerequis);

    }

    /**
     * afficher les boutons "se déconnecter", "modifier son mot de passe" et
     * "modifier son identifiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son mot de passe" et
     * "modifier son identifiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * revenir à la page DirAccueilController
     *
     * @throws IOException
     */
    public void retour() throws IOException {
        Stage source = (Stage) buttonRetour.getScene().getWindow();
        source.close();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/DirAccueil.fxml"));
        Parent root = loader.load();

        DirAccueilController pageDirAccueil = loader.getController();
        pageDirAccueil.setRepertoire(repertoire);
        pageDirAccueil.setFichierEtudiant(fichierEtudiant);
        pageDirAccueil.setFichierEnseignement(fichierEnseignement);
        pageDirAccueil.setFichierMention(fichierMention);
        pageDirAccueil.setFichierParcours(fichierParcours);
        pageDirAccueil.initialiserLesRestaure();
        pageDirAccueil.listeEtudiants();
        pageDirAccueil.initialiserComboBox();

        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * remmettre les valeurs initiale des comboBox
     */
    public void vider() {
        comboBoxMention.setValue("Mention");
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");

        listeEUPrerequis();
    }
}
