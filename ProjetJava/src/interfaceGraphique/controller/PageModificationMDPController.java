package interfaceGraphique.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author laura, océane, carlos
 */
public class PageModificationMDPController implements Initializable {

    private static String fichierIdentifiant;
    private static String fichierTemp;

    // ComboBox
    @FXML
    private ComboBox<String> comboBoxStatus;
    ObservableList<String> comboBoxStatusList = (ObservableList<String>) FXCollections
            .observableArrayList("Directeur d'étude", "Secrétariat pédagogique", "Bureau d'examen");

    // Boutons
    @FXML
    private Button buttonValiderIdentifiant;
    @FXML
    private Button buttonValiderNewMdp;
    @FXML
    private Button buttonRetourAccueil;

    // Panel
    @FXML
    private AnchorPane panelModifMdp;
    @FXML
    private AnchorPane panelMessageConfirmation;

    // TextField
    @FXML
    private TextField textFieldIdentifiant;
    @FXML
    private TextField textFieldNewMdp;

    // Label
    @FXML
    private Label labelCompteInexistant;
    @FXML
    private Label labelMessage;
    @FXML
    private Label labelIdentifiantManquant;
    @FXML
    private Label labelMDPManquant;

    private String statut;
    private String identifiant;
    private String newMdp;

    /**
     * Initialisation du comboBox avec les valeurs Directeur d'étude Scrétariat
     * pédagogique Bureau d'étude
     *
     * Cache certain panel
     *
     * @param arg0 (url)
     * @param arg1 (ResourceBundle)
     */
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        comboBoxStatus.setValue("Directeur d'étude");
        comboBoxStatus.setItems(comboBoxStatusList);

        labelCompteInexistant.setVisible(false);
        panelMessageConfirmation.setVisible(false);
        labelIdentifiantManquant.setVisible(false);
        labelMDPManquant.setVisible(false);
        fichierIdentifiant = getPath() + "Données/identifiants.csv";
        fichierTemp = getPath() + "Données/temp.csv";
    }

    /**
     * Si l'identifiant existe pour un statut donné Affiche un panel pour saisir
     * un nouveau mot de passe
     */
    @FXML
    public void afficherPanelModificationMDP() {
        identifiant = textFieldIdentifiant.getText();
        statut = (String) comboBoxStatus.getSelectionModel().getSelectedItem();
        labelIdentifiantManquant.setVisible(false);

        if (identifiant.equals("")) {
            labelIdentifiantManquant.setVisible(true);
        } else {
            boolean correcte = false;
            File fichier = new File(fichierIdentifiant);
            if (fichier.exists()) {
                // Le fichier existe
                labelCompteInexistant.setVisible(false);
                try {
                    // On vérifie que l'identifiant et le statut existe
                    //BufferedReader r = new BufferedReader(new FileReader(fichierIdentifiant));
                    BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(fichierIdentifiant), "UTF-8")); // lis ligne par ligne

                    String l;
                    while ((l = r.readLine()) != null) {
                        String[] ligne = l.split(";");
                        if (ligne[0].equals(identifiant) && ligne[2].equals(statut)) { // Il existe
                            textFieldIdentifiant.setEditable(false);
                            comboBoxStatus.setEditable(false);
                            panelModifMdp.setVisible(true);
                            correcte = true;
                            break;
                        }
                    }
                    r.close();
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }

            if (!correcte) {
                // Si le fichier n'existe pas : afficher message pour informer l'utilisateur que son compte n'existe pas
                labelCompteInexistant.setVisible(true);
            }
        }
    }

    /**
     * Permet de modifier le mot de passe Recopie fichier identifiants.csv dans
     * un fichier temporaire en prenant en compte le nouveau mot de pass
     * Supprime identifiants.csv Modifie nom du fichier temporaire en
     * identifiants.csv
     *
     * @throws IOException
     */
    @FXML
    public void modifierMDP() throws IOException {
        identifiant = textFieldIdentifiant.getText();
        newMdp = textFieldNewMdp.getText();
        labelMDPManquant.setVisible(false);

        if (newMdp.equals("")) {
            labelMDPManquant.setVisible(true);
        } else {
           
            try {
                // FileWriter w = new FileWriter(fichierTemp);
                //BufferedReader r = new BufferedReader(new FileReader(fichierIdentifiant));
                BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(fichierIdentifiant), "UTF-8")); // lis ligne par ligne
                String l;
                
                StringBuilder sb = new StringBuilder();
                while ((l = r.readLine()) != null) {
                    String[] ligne = l.split(";");
                    if (ligne[0].trim().equals(identifiant.trim()) && ligne[2].equals(statut)) { // ligne à recopier avec modification
                        sb.append(ligne[0]);
                        sb.append(";");
                        sb.append(newMdp);
                        sb.append(';');
                        sb.append(ligne[2]);
                        sb.append("\n");
                        //wr.write(ligne[0] + ";" + newMdp + ";" + ligne[2] + "\n");
                    } else { // ligne à recopier sans modifier
                       // wr.write(ligne[0] + ";" + ligne[1] + ";" + ligne[2] + "\n");
                       sb.append(ligne[0]);
                       sb.append(';');
                       sb.append(ligne[1]);
                       sb.append(';');
                       sb.append(ligne[2]);
                       sb.append("\n");
                    }
                    
                }
                OutputStream out = new FileOutputStream(fichierIdentifiant);
                Writer wr = new OutputStreamWriter(out, "UTF-8");
                wr.append(sb.toString());
                wr.close();
                r.close();
                
               

                panelMessageConfirmation.setVisible(true);
                labelMessage.setText("Bonjour " + identifiant + "\nVotre nouveau mot de passe a bien été enregistré");

            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }

    /**
     * Permet de revenir à la page d'accueil
     *
     * @throws IOException
     */
    @FXML
    public void pageAccueil() throws IOException {
        Stage source = (Stage) buttonRetourAccueil.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    private static String getPath() {
        String path = PageModificationMDPController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PageCreerCompteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE IDENTIFIANT: "+absolutePath);
        return absolutePath;
    }

}
