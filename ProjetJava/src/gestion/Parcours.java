package gestion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Décrit un Parcours d'une Mention
 *
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Parcours extends Mention {

    private String nomP;
    private ArrayList<Enseignement> enseignementsP;
    private final static String CHEMINDEFAULT = "Données/parcours.csv";

    /**
     * Définit un Parcours attention !! Si ont utilise cette méthode ont ne peux
     * pas recupérer les ue du parents. Ce constructeur sert UNIQUEMENT pour la
     * sauvegarde avec le nomM (nom de la Mention, String) et le nom du Parcours
     * (String)
     *
     * @param nomM (String)
     * @param nomP (String)
     */
    private Parcours(String nomM, String nomP) {
        super(nomM);
        this.nomP = nomP;
        this.enseignementsP = new ArrayList<>();
    }

    /**
     * Définit un Parcours avec le nomM (nom de la Mention, String), la liste
     * des Enseignements (ArrayList&lt;Enseignement&gt;), et le nom du Parcours
     * (String)
     *
     * @param nomM (String)
     * @param ue (ArrayList&lt;Enseignement&gt;)
     * @param nomP (String)
     */
    public Parcours(String nomM, ArrayList<Enseignement> ue, String nomP) {
        super(nomM, ue);
        this.nomP = nomP;
        this.enseignementsP = new ArrayList<>();
    }

    /**
     * Permet de récupérer le nom d'un Parcours
     *
     * @return nomP
     */
    public String getNomP() {
        return nomP;
    }

    /**
     * Permet de modifier le nomP d'un Parcours
     *
     * @param nomP (String)
     */
    public void setNomP(String nomP) {
        this.nomP = nomP;
    }

    /**
     * Permet de recupérer la liste des enseignements d'un parcours
     * (ArrayList&lt;Enseignement&gt;)
     *
     * @return (ArrayList&lt;Enseignement&gt;) enseignementsP
     */
    public ArrayList<Enseignement> getEnseignementsP() {
        return enseignementsP;
    }

    /**
     * Retourne tous les enseignements d'un Parcours (avec ceux de sa Mention)
     *
     * @param restaure (ArrayList&lt;Parcours&gt;)
     * @param nomParcours (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public static ArrayList<Enseignement> listeEnsParcours(ArrayList<Parcours> restaure, String nomParcours) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> e = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                if (restaure.get(i).getNomP().equals(nomParcours)) {
                    e.addAll(restaure.get(i).getEnseignements());
                    e.addAll(restaure.get(i).getEnseignementsP());
                }
            }
            return e;
        }
        return null;

    }

    /**
     * Retourne tous les enseignements UNIQUEMENT d'un Parcours
     *
     * @param restaure (ArrayList&lt;Parcourst&gt;)
     * @param nomParcours (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public static ArrayList<Enseignement> listeEnsParcoursSansMention(ArrayList<Parcours> restaure, String nomParcours) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> e = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                if (restaure.get(i).getNomP().equals(nomParcours)) {
                    e.addAll(restaure.get(i).getEnseignementsP());
                }
            }
            return e;
        }
        return null;
    }

    /**
     * Renvoie le nom des Enseignements d'un Parcours donné
     *
     * @param restaure
     * @param nomParcours
     * @return
     */
    public static ArrayList<String> nomEnseignementPourParcours(ArrayList<Parcours> restaure, String nomParcours) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> ens = listeEnsParcoursSansMention(restaure, nomParcours);
            ArrayList<String> laListe = new ArrayList<>();
            for (Enseignement ue : ens) {
                laListe.add(ue.getNom());
            }
            return laListe;

        }

        return null;
    }

    /**
     * Renvoie les codes des Enseignements d'un Parcours donné
     *
     * @param restaure
     * @param nomParcours
     * @return
     */
    public static ArrayList<String> codeEnseignementPourParcours(ArrayList<Parcours> restaure, String nomParcours) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> ens = listeEnsParcoursSansMention(restaure, nomParcours);
            ArrayList<String> laListe = new ArrayList<>();
            for (Enseignement ue : ens) {
                laListe.add(ue.getCode());
            }
            return laListe;

        }

        return null;
    }

    /**
     * Retourne tous les enseignements d'une Mention et de ces parcours
     *
     * @param restaure (ArrayList&lt;Parcourst&gt;)
     * @param nomMention (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public static ArrayList<Enseignement> listeEnsMention(ArrayList<Parcours> restaure, String nomMention) {
        int k = 0;
        ArrayList<Enseignement> e = new ArrayList<>();
        //on parcourt la liste des parcours
        for (int i = 0; i < restaure.size(); i++) {
            //si le nom de la mention est bon
            if (restaure.get(i).getNom().equals(nomMention)) {
                //si c'est la première fois qu'on rentre dans le if
                if (k == 0) {
                    //on enregistre les enseignements de la mention
                    e.addAll(restaure.get(i).getEnseignements());
                }
                //on parcourt la liste d'enseignements du parcours
                for (int j = 0; j < restaure.get(i).getEnseignementsP().size(); j++) {
                    //si l'enseignement n'est pas déjà enregistré
                    if (!e.contains(restaure.get(i).getEnseignementsP().get(j))) {
                        //on enregistre l'enseignement
                        e.add(restaure.get(i).getEnseignementsP().get(j));
                    }
                }
                k++;
            }
        }
        return e;
    }

    /**
     * Retourne la liste des parcours
     *
     * @param restaure (ArrayList&lt;Parcoursg&gt;)
     * @return (ArrayList&lt;String&gt;)
     */
    public static ArrayList<String> nomParcours(ArrayList<Parcours> restaure) {
        ArrayList<String> nom = new ArrayList<>();
        for (int i = 0; i < restaure.size(); i++) {
            nom.add(restaure.get(i).nomP);
        }
        return nom;
    }

    /**
     * Retourne la liste des parcours d'une mention
     *
     * @param restaure (ArrayList&lt;Parcourst&gt;)
     * @param nomMention (String)
     * @return (ArrayList&lt;String&gt;)
     */
    public static ArrayList<String> nomParcoursMention(ArrayList<Parcours> restaure, String nomMention) {
        if (!restaure.isEmpty()) {
            ArrayList<String> nom = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                if (restaure.get(i).getNom().equals(nomMention)) {
                    nom.add(restaure.get(i).nomP);
                }
            }
            return nom;
        }
        return null;

    }

    /**
     *
     * @param restaure
     * @param restaureParcours
     * @param e
     * @return
     */
    public static String mentionPourUE(ArrayList<Mention> restaure, ArrayList<Parcours> restaureParcours, Enseignement e) {
        if (!restaure.isEmpty()) {
            if (!restaure.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (Mention m : restaure) {
                    ArrayList<Enseignement> ensMent = listeEnsMention(restaureParcours, m.getNom());
                    for (Enseignement ens : ensMent) {
                        if (ens.getCode().equals(e.getCode()) && sb.length() != 0) {
                            sb.append(", ");
                            sb.append(m.getNom());
                            break;
                        }
                        if (ens.getCode().equals(e.getCode()) && sb.length() == 0) {
                            sb.append(m.getNom());
                            break;

                        }

                    }

                }
                // A ce stade on a les 

                return sb.toString();

            }
        }
        return null;
    }

    /**
     * Renvoie une chaine de carecter composée de tout les parcours correspondant à un enseignement 
     * @param restaure (ArrayList&lt;Mention&gt;)
     * @param restaureParcours (ArrayList&lt;Parcours&gt;)
     * @param e (Enseignement)
     * @return (String)
     */
    public static String parcoursPourUE(ArrayList<Mention> restaure, ArrayList<Parcours> restaureParcours, Enseignement e) {
        if (!restaureParcours.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (Parcours p : restaureParcours) {
                ArrayList<Enseignement> ensMent = listeEnsMention(restaureParcours, p.getNom());

                
                for (Enseignement ens : ensMent) {
                    if (ens.getCode().equals(e.getCode()) && sb.length() != 0) {
                        sb.append(", ");
                        sb.append(p.getNomP());
                        break;
                    }
                    if (ens.getCode().equals(e.getCode()) && sb.length() == 0) {
                        sb.append(p.getNomP());
                        break;

                    }

                }

            }
            return sb.toString();

        }
        return null;
    }

    /**
     * Permet d'ajouter un Enseignement a un Parcours
     *
     * @param ue (Enseignement)
     */
    @Override
    public void ajoutEnseignement(Enseignement ue) {
        if (this.enseignementsP != null) {
            if (!this.enseignementsP.contains(ue)) {
                this.enseignementsP.add(ue);
            } else {
                System.out.println("Enseignement déjà dans la liste !");
            }
        } else {
            this.enseignementsP.add(ue);
        }

    }

    private static String getPath() {
        String path = Parcours.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE ENSEIGNEMENT: "+absolutePath);
        return absolutePath;

    }

    @Override
    public void sauvegarde() {
        StringBuilder sb = new StringBuilder();
        StringBuilder tout = new StringBuilder();
        boolean dejaDansFich = false, uneModification = false;
        String[] colonne;
        String path = getPath();
        // Vérifie si le répertoire Données existe sinon le crée
        File directory = new File(path + "Données");
        if (!directory.exists()) {
            directory.mkdir();
        }
        File f = new File(path + CHEMINDEFAULT);
        if (!f.exists()) {
            if (!f.isDirectory()) {
                sb.append('\ufeff');
                sb.append("NomM");
                sb.append(';');
                sb.append("Enseignements");
                sb.append(';');
                sb.append("NomP");
                sb.append('\n');
            }
        }
        sb.append(Parcours.super.getNom());
        sb.append(';');
        if (enseignementsP.isEmpty()) {
            sb.append("[]");
            sb.append(';');
        } else {
            // Permet d'avoir un format plus facile à traiter
            // récuperer les enseignements
            for (int i = 0; i < enseignementsP.size(); i++) {
                sb.append("[");
                sb.append(enseignementsP.get(i).getCode());
                //sb.append(" , ");
                //sb.append(enseignementsP.get(i).getCredit());
                sb.append("]");
            }
            sb.append(';');
        }
        sb.append(nomP);

        BufferedReader br;
        if (f.exists()) {
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(path + CHEMINDEFAULT), "UTF-8"));
                String ligne;

                while ((ligne = br.readLine()) != null) {
                    colonne = ligne.split(";");
                    if (ligne.equals(sb.toString())) {
                        dejaDansFich = true;
                    } else {
                        if (colonne[2].equals(nomP)) {
                            //System.out.println("ligne a modif");
                            tout.append(sb.toString());
                            tout.append("\n");
                            dejaDansFich = true;
                            uneModification = true;

                        } else {
                            tout.append(ligne);
                            tout.append("\n");
                        }

                    }
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Parcours.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Parcours.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!dejaDansFich) {
            try {
                try (FileWriter writer = new FileWriter(path + CHEMINDEFAULT)) {
                    sb.append("\n");
                    tout.append(sb.toString());
                    writer.append(tout);
                    writer.flush();
                    writer.close();
                    //System.out.println("done!");
                }
            } catch (IOException ex) {
                Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (dejaDansFich) {
                if (!uneModification) {
                    //System.out.println("Déjà dans le fichier ! ");
                } else {
                    if (uneModification) {
                        try {
                            try (FileWriter writer = new FileWriter(path + CHEMINDEFAULT)) {
                                writer.append(tout);
                                writer.flush();
                                writer.close();
                                //System.out.println("done!");;;
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

    }

    /**
     * Retourne une ArrayList&lt;Parcours&gt; contenue dans le fichier contenue
     * dans les données du projet
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param listeMenRestaurer (ArrayList&lt;Mention&gt;)
     * @return (ArrayList&lt;Mention&gt;)
     */
    public static ArrayList<Parcours> restaureP(ArrayList<Enseignement> listeEuRestaurer,
            ArrayList<Mention> listeMenRestaurer) {
        return Parcours.restaureP(listeEuRestaurer, listeMenRestaurer, getPath() + CHEMINDEFAULT);
    }

    /**
     * Retourne une ArrayList&lt;Parcours&gt; contenue dans le fichier dont le
     * chemin et donner en paramètre !! les slashs doivent être doublée.
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param listeMenRestaurer (ArrayList&lt;Mentiont&gt;)
     * @param chemin (String sous forme C:/lechemin/estvoila/fichier.csv)
     * @return ArrayList&lt;Mention&gt;
     */
    public static ArrayList<Parcours> restaureP(ArrayList<Enseignement> listeEuRestaurer,
            ArrayList<Mention> listeMenRestaurer, String chemin) {
        if (listeEuRestaurer == null) {
            System.out.println("Liste Enseignements restaurer est vide !");
            return null;
        }
        if (listeMenRestaurer == null) {
            System.out.println("Liste Mentions retaurer est vide ! ");
            return null;
        }
        File f = new File(chemin);
        BufferedReader br;
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    br = new BufferedReader(new InputStreamReader(new FileInputStream(chemin), "UTF-8"));
                    String ligne, sansPare;
                    String[] colonne;
                    String[] verif;
                    boolean formatCorrect = false;
                    // A PASSER EN PARAM POUR PAS RECALCULER A CHAQUE FOIS??
                    //ArrayList<Enseignement> listeEu = Enseignement.restaure();
                    //ArrayList<Mention> listeMention = Mention.restaure();
                    ArrayList<Parcours> liste = new ArrayList<>();

                    while ((ligne = br.readLine()) != null) {

                        colonne = ligne.split(";");
                        //transforme les [] en "" 
                        sansPare = colonne[1];
                        sansPare = sansPare.substring(1); // enleve le premier [
                        sansPare = sansPare.substring(0, sansPare.length() - 1);// enleve le dernier ]

                        if ((!colonne[0].equals("NomM") || !colonne[0].equals("\ufeffNomM") || !colonne[0].equals("ï»¿NomM"))
                                && !colonne[1].equals("Enseignements")
                                && !colonne[2].equals("NomP") && formatCorrect) {

                            ArrayList<Enseignement> listeEnsMention = Mention.ensMention(listeMenRestaurer, colonne[0]);
                            if (!listeEnsMention.isEmpty()) {
                                liste.add(new Parcours(colonne[0], listeEnsMention, colonne[2]));
                            } else {
                                liste.add(new Parcours(colonne[0], colonne[2]));
                            }

                            if (!"".equals(sansPare)) {
                                // il y a un prerequis a ajouter
                                verif = sansPare.split("]");

                                for (int i = 0; i < verif.length; i++) {
                                    // Rajoute les UE dans les mentions s'ils sont enregistrer dans la liste des enseignements
                                    for (int j = 0; j < listeEuRestaurer.size(); j++) {
                                        if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim())) {
                                            liste.get(liste.size() - 1).ajoutEnseignement(listeEuRestaurer.get(j));
                                        } else {
                                            if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim().substring(1))) {
                                                liste.get(liste.size() - 1).ajoutEnseignement(listeEuRestaurer.get(j));
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if ((colonne[0].equals("\ufeffNomM") || colonne[0].equals("NomM") || colonne[0].equals("ï»¿NomM")) && colonne[1].equals("Enseignements")
                                && colonne[2].equals("NomP")) {
                            formatCorrect = true;
                        }

                    }
                    if (!formatCorrect) {
                        //System.out.println("Le format des colonnes n'est pas correcte !");
                        return null;
                    }
                    return liste;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            // System.out.println("Fichier n'existe pas");
            return null;
        }
        //System.out.println("Ce n'est pas un fichier");
        return null;
    }

    @Override
    public String toString() {
        return nomP;
    }

}
