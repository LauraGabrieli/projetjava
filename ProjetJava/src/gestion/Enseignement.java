package gestion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Décrit un Enseignement (ou UE)
 *
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Enseignement implements Fichier {

    private String nom;
    private String code;
    private int credit;
    private ArrayList<Enseignement> prerequis;
    private final static String CHEMINDEFAULT = "Données/enseignements.csv";

    /**
     * Définit un Enseignement avec sont code (String) et ces crédits (int)
     *
     * @param nom (String)
     * @param code (String)
     * @param credit (int)
     */
    public Enseignement(String nom, String code, int credit) {
        this.nom = nom;
        this.code = code;
        if (credit != 3 && credit != 6) {
            throw new IllegalArgumentException("Valeur 3 ou 6");
        } else {
            this.credit = credit;
        }
        this.prerequis = new ArrayList<>();

    }

    /**
     * Définit un Enseignement avec sont code (String), ces crédits (int) et la
     * liste des enseignements prerequis (ArrayList&lt;Enseignement&gt;)
     *
     * @param nom (String)
     * @param code (String)
     * @param credit (int)
     * @param prerequis (ArrayList&lt;Enseignement&gt;)
     */
    public Enseignement(String nom, String code, int credit, ArrayList<Enseignement> prerequis) {
        this.nom = nom;
        this.code = code;
        this.credit = credit;
        this.prerequis = prerequis;
    }

    /**
     * Permet de récupérer le nom d'un Enseignement
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de récupérer le code d'un Enseignement
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * Permet de récupérer le credit d'un Enseignement
     *
     * @return credit
     */
    public int getCredit() {
        return credit;
    }

    /**
     * Permet de récupérer un ensemble d'Enseignement prérequis pour effectuer
     * l'Enseignement présent
     *
     * @return ArrayList&lt;Enseignement&gt;
     */
    public ArrayList<Enseignement> getPrerequis() {
        return prerequis;
    }

    /**
     * Permet de modifier le nom d'un Enseignement
     *
     * @param nom (String)
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Permet de modifier le code d'un Enseignement
     *
     * @param code (String)
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Permet de modifier le credit d'un Enseignement
     *
     * @param credit (int)
     */
    public void setCredit(int credit) {
        this.credit = credit;
    }

    /**
     * Permet de définir la liste des Enseignements prérequis pour
     * l'Enseignement présent
     *
     * @param prerequis (ArrayList&lt;Enseignement&gt;)
     */
    public void setPrerequis(ArrayList<Enseignement> prerequis) {
        this.prerequis = prerequis;
    }

    /**
     * Permet d'ajouter un prerequis a un Enseignement
     *
     * @param ue (Enseignement)
     */
    public void addPrerequis(Enseignement ue) {
        if (!this.prerequis.contains(ue)) {
            this.prerequis.add(ue);
        } else {
            System.out.println("Déjà dans les prérequis! ");
        }

    }

    /**
     * Retourne true si l'enseignement est un enseignement de découverte
     *
     * @return ArrayList&lt;Enseignement&gt;
     */
    public boolean isDecouverte() {
        return (this.code.contains("UEDEC"));
    }

    /**
     * retourne la liste des noms de tous les enseignements
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @return ArrayList&lt;Stringt&gt;
     */
    public static ArrayList<String> listeNomEns(ArrayList<Enseignement> restaure) {

        if (!restaure.isEmpty()) {
            ArrayList<String> e = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                e.add(restaure.get(i).getNom());
            }
            return e;
        }
        return null;
    }

    /**
     * Renvoie la liste des Enseignements qui ont pour nom celui en parametre
     * (peut y en avoir plusieurs)
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param nom (String)
     * @return ArrayList&lt;Enseignement&gt;
     */
    public static ArrayList<Enseignement> listeEnsPourNom(ArrayList<Enseignement> restaure, String nom) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> liste = new ArrayList<>();
            for (Enseignement ens : restaure) {
                if (ens.getNom().equals(nom)) {
                    liste.add(ens);
                }
            }
            return liste;
        }
        return null;
    }

    /**
     * Renvoie la liste des enseignements qui ont pour code celui en parametre
     * (normalement c'est une liste d'un élément)
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param code (String)
     * @return ArrayList&lt;Enseignement&gt;
     */
    public static ArrayList<Enseignement> listeEnsCode(ArrayList<Enseignement> restaure, String code) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> liste = new ArrayList<>();
            for (Enseignement ens : restaure) {
                if (ens.getCode().equals(code)) {
                    liste.add(ens);
                }
            }
            return liste;

        }
        return null;
    }

    /**
     * Retourne l'Enseignement dont le code et passé en parametre
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param code (String)
     * @return (Enseignement)
     */
    public static Enseignement listeEnsCodeBis(ArrayList<Enseignement> restaure, String code) {
        if (!restaure.isEmpty()) {
            for (Enseignement ens : restaure) {
                if (ens.getCode().equals(code)) {
                    return ens;
                }
            }

        }
        return null;
    }

    /**
     * Renvoie une liste de string des codes de tous les enseignements
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @return ArrayList&lt;String&gt;
     */
    public static ArrayList<String> listeEnsCodeString(ArrayList<Enseignement> restaure) {
        if (!restaure.isEmpty()) {
            ArrayList<String> liste = new ArrayList<>();
            for (Enseignement ens : restaure) {
                liste.add(ens.getCode());
            }
            return liste;

        }
        return null;
    }

    /**
     * Renvoie sous forme de string le code d'un Enseignement qui a le code en
     * parametre
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param ue (String)
     * @return (ArrayList&lt;String&gt;)
     */
    public static ArrayList<String> listeEnsCodeStringCodeDonne(ArrayList<Enseignement> restaure, String ue) {
        if (!restaure.isEmpty()) {
            ArrayList<String> liste = new ArrayList<>();
            for (Enseignement ens : restaure) {
                if (ens.getNom().equals(ue)) {
                    liste.add(ens.getCode());
                }

            }
            return liste;

        }
        return null;
    }

    /**
     * Renvoie une liste d'entier correspondant aux nombres d'étudiant par
     * Enseignement
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param restaureEtu (ArrayList&lt;Etudiant&gt;)
     * @return (ArrayList&lt;Integer&gt;)
     */
    public static ArrayList<Integer> nbEtuSuisEns(ArrayList<Enseignement> restaure, ArrayList<Etudiant> restaureEtu) {
        if (!restaure.isEmpty()) {
            if (!restaureEtu.isEmpty()) {
                int indexArray = 0;
                ArrayList<Integer> EtuSuisEns = new ArrayList<>();
                //Parcours tout les enseignements et tout les etudiants
                EtuSuisEns.add(indexArray, 0);
                for (Enseignement ens : restaure) {
                    EtuSuisEns.add(indexArray, 0);
                    for (Etudiant etu : restaureEtu) {
                        //Si l'étudiant suis l'enseignemment ajouter dans l'arraylist +1
                        if (etu.suitEns(ens)) {
                            EtuSuisEns.set(indexArray, (EtuSuisEns.get(indexArray)) + 1);
                        }
                    }
                    indexArray += 1;

                }
                //enleve la derniere valeur qui est en trop
                EtuSuisEns.remove(EtuSuisEns.size() - 1);
                return EtuSuisEns;

            }
        }

        return null;

    }

    /**
     * Renvoie le nombre d'étudiant pour un enseignement donnée
     *
     * @param restaureEtu (ArrayList&lt;Etudiant&gt;)
     * @param ue (Enseignement)
     * @return (int)
     */
    public static int nbEtuSuisUnEns(ArrayList<Etudiant> restaureEtu, Enseignement ue) {
        if (!restaureEtu.isEmpty()) {
            int nbEleve = 0;
            for (Etudiant etu : restaureEtu) {
                if (etu.suitEns(ue)) {
                    nbEleve += 1;
                }
            }
            return nbEleve;
        }

        return 0;
    }

    /**
     * Permet de predire le nombre d'étudiants qui seront potentiellement inscrit l'année n+1 
     * en comptant le nombre potentiel de redoublant et le nombre d'étudiant ayant potentiellement les
     * prérequis
     * 
     * @param restaureEtu (ArrayList&lt;Etudiant&gt;)
     * @param restaureEns (ArrayList&lt;Enseignement&gt;)
     * @param ue (Enseignement)
     * @return (int)
     */
    public static int nbEtuSuisUnEnsPrevisionnel(ArrayList<Etudiant> restaureEtu, ArrayList<Enseignement> restaureEns, Enseignement ue) {

        if (!restaureEtu.isEmpty()) {
            if (!restaureEns.isEmpty()) {
                float tauxReussite = Enseignement.tauxDeReussiteEns(restaureEtu, restaureEns, ue);
               // System.out.println("taux : "+tauxReussite);

                //j'aurais pu simplifier en affectant directement les valeurs mais pour que ça soit plus
                // facile a lire j'ai décomposer
                int nbEtuTotal = 0, nbNouveauEtu = 0;
                int nbEleveRedoublantPotentiel = (int) ((Enseignement.nbEtuSuisUnEns(restaureEtu, ue)) * ((100-tauxReussite)/ 100)) ;
                // int nbEleveRedoublantPotentiel = (int) ((Enseignement.nbEtuSuisUnEns(restaureEtu, ue)) * (100)) ;

              //System.out.println("NB REDOUBLANT "+nbEleveRedoublantPotentiel);
                nbEtuTotal += nbEleveRedoublantPotentiel;

                ArrayList<Enseignement> listePrerequis = ue.getPrerequis();
                for(Etudiant etu : restaureEtu){
                    int nbSuisEns=0; // DOIT ETRE EGALE AUX LENGTH DE LA LISTE DE PREREQUIS
                    for(Enseignement ens : listePrerequis){
                        if(etu.suitEns(ens) || etu.aValideEns(ens)){
                            nbSuisEns+=1;
                        }
                    }
                    //System.out.println("nbSuisEns"+nbSuisEns);
                    
                    if(nbSuisEns==listePrerequis.size() && !etu.aValideEns(ue) &&!etu.suitEns(ue)){
                        nbNouveauEtu+=1;
                    }
                    
                }
              
                //System.out.println("NB NOUVEAU : "+nbNouveauEtu);
                nbEtuTotal = nbEleveRedoublantPotentiel + nbNouveauEtu;
                return nbEtuTotal;
            }
        }
        return 0;

    }

    /**
     * Renvoie le taux de reussite pour un enseignement donnée pour l'ensemble des années sauf l'année en cours
     * 
     * @param restaureEtu (ArrayList&lt;Etudiant&gt;)
     * @param restaureEns (ArrayList&lt;Enseignement&gt;)
     * @param ue (Enseignement)
     * @return (float)
     */
    public static float tauxDeReussiteEns(ArrayList<Etudiant> restaureEtu, ArrayList<Enseignement> restaureEns, Enseignement ue) {
        if (!restaureEtu.isEmpty()) {
            if (!restaureEns.isEmpty()) {
                // Liste des etudiant actuellement entrain de suivre un ens
                ArrayList<Etudiant> listeEtuSuivantUE1 = Etudiant.listeEtudCode(restaureEtu, restaureEns, ue.getCode());

                // eleve suivant actuellement l'enseignement mais qui l'avais déjà 
                // essayer avant
                ArrayList<Etudiant> listeEtuSuivantUE = new ArrayList<>();
                AnneeUniv an = new AnneeUniv();

                if (!listeEtuSuivantUE1.isEmpty()) {
                    for (Etudiant etu0 : listeEtuSuivantUE1) {
                        ArrayList<Suivi> listeSuiviEtu = etu0.getListeSuivi();
                        for (Suivi s0 : listeSuiviEtu) {
                            if (s0.getEnseignement().getCode().equals(ue.getCode())) {
                                if (an.getAnnee().equals(s0.getAnnee().getAnnee())) {
                                    listeEtuSuivantUE.add(etu0);
                                }

                            }
                        }
                    }
                }

                // Récuperer tout les etudiants pour année n-1 abandonné on fait sur la totalité -ceux de maintenannt
                // String anneeNMoins=String.valueOf(Calendar.getInstance().get(Calendar.YEAR)-2)+"/"+String.valueOf(Calendar.getInstance().get(Calendar.YEAR)-1);
                // liste des etudiant ayant suivi validée ou non l'ens
                ArrayList<Etudiant> listeEtuAyantSuiviUE = new ArrayList<>();

                for (Etudiant etu : restaureEtu) {
                    ArrayList<Suivi> listeSuiviEtu = etu.getListeSuivi();
                    for (Suivi s1 : listeSuiviEtu) {
                        if (s1.getEnseignement().getCode().equals(ue.getCode())) {
                            listeEtuAyantSuiviUE.add(etu);
                        }
                    }
                }
                Iterator<Etudiant> it = listeEtuSuivantUE.iterator();
                while (it.hasNext()) {
                    Etudiant i = it.next();
                    Iterator<Etudiant> etua= listeEtuAyantSuiviUE.iterator();
                    while(etua.hasNext()){
                        Etudiant j=etua.next();
                        if (i.getNumE() == (j.getNumE())) {
                           
                            etua.remove();
                            break;
                        }
                    }
                   

                }
                
                int nbReussi = 0,nbTotal=0;
                if (listeEtuAyantSuiviUE.isEmpty()) {
                    return 0;
                }
                for(Etudiant etu:listeEtuAyantSuiviUE){
                    ArrayList<Suivi> listeSuiviEtu = etu.getListeSuivi();
                    for (Suivi s1 : listeSuiviEtu) {
                        if (s1.getEnseignement().getCode().equals(ue.getCode())) {
                            nbTotal+=1;
                        }
                    }
                }

                for (Etudiant etu1 : listeEtuAyantSuiviUE) {
                    if (etu1.aValideEns(ue)) {
                        nbReussi += 1;
                    }

                }
                

                float taux;

                taux = (float)(100 * nbReussi) / nbTotal;
                return taux;
            }
        }

        return 0;
    }

    /**
     * Renvoie une liste (clé, valeur) correspondant à un enseignement et le
     * nombre d'étudiant correspondant
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param restaureEtu (ArrayList&lt;Etudiant&gt;)
     * @return (HashMap&lt;Enseignement,Integer&gt;)
     */
    public static HashMap<Enseignement, Integer> nbEtuSuisEnsHashMap(ArrayList<Enseignement> restaure, ArrayList<Etudiant> restaureEtu) {
        if (!restaure.isEmpty()) {
            if (!restaureEtu.isEmpty()) {
                int indexArray = 0;
                ArrayList<Integer> EtuSuisEns = new ArrayList<>();
                //Parcours tout les enseignements et tout les etudiants
                EtuSuisEns.add(indexArray, 0);
                for (Enseignement ens : restaure) {
                    EtuSuisEns.add(indexArray, 0);
                    for (Etudiant etu : restaureEtu) {
                        //Si l'étudiant suis l'enseignemment ajouter dans l'arraylist +1
                        if (etu.suitEns(ens)) {
                            EtuSuisEns.set(indexArray, (EtuSuisEns.get(indexArray)) + 1);
                        }
                    }
                    indexArray += 1;

                }
                //enleve la derniere valeur qui est en trop
                EtuSuisEns.remove(EtuSuisEns.size() - 1);
                HashMap<Enseignement, Integer> test = new HashMap<>();
                for (int i = 0; i < restaure.size(); i++) {
                    //System.out.println("restaure : "+i+" "+restaure.get(i));
                    //System.out.println("etu : "+i+" "+EtuSuisEns.get(i));
                    test.put(restaure.get(i), EtuSuisEns.get(i));
                }

                return test;

            }
        }

        return null;

    }

    /**
     * Renvoie le niveau d'un Enseignement donnée
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param e (Enseignement)
     * @return (int)
     */
    public static int niveauEnseignement(ArrayList<Enseignement> restaure, Enseignement e) {
        if (!restaure.isEmpty()) {
            int niveau = 0;
            Enseignement ue2 = null;
            ArrayList<Enseignement> ens = e.getPrerequis();
            if (ens.isEmpty()) {
                return niveau; // Vaut 0 si il n'y a pas de prérequis
            } else {
                niveau += 1; // Vaut au moins 1 car il y'a un prérequis
                for (Enseignement ue : ens) {
                    for (Enseignement tous : restaure) {
                        if (tous.getCode().equals(ue.getCode())) {
                            ue2 = tous;
                            //System.out.println("ue2"+ue2);
                        }
                    }
                    if (ue2 != null) {
                        int testNiveau = niveauEnseignement(restaure, ue2);
                        //System.out.println("testNiveau :" + testNiveau);
                        while (testNiveau >= niveau) {
                            niveau += 1;
                        }

                    }

                }

                return niveau;
            }
        }
        return -1; // si y'a eu une erreur
    }

    /**
     * Renvoie le niveau d'un Enseignant dont on connais le code
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param code (String)
     * @return (int)
     */
    public static int niveauEnseignementCode(ArrayList<Enseignement> restaure, String code) {
        if (!restaure.isEmpty()) {
            int niveau = 0;
            Enseignement ue2 = null;
            Enseignement e = listeEnsCodeBis(restaure, code);
            ArrayList<Enseignement> ens = e.getPrerequis();
            if (ens.isEmpty()) {
                return niveau; // Vaut 0 si il n'y a pas de prérequis
            } else {
                niveau += 1; // Vaut au moins 1 car il y'a un prérequis
                for (Enseignement ue : ens) {
                    for (Enseignement tous : restaure) {
                        if (tous.getCode().equals(ue.getCode())) {
                            ue2 = tous;
                            //System.out.println("ue2"+ue2);
                        }
                    }
                    if (ue2 != null) {
                        int testNiveau = niveauEnseignement(restaure, ue2);
                        //System.out.println("testNiveau :" + testNiveau);
                        while (testNiveau >= niveau) {
                            niveau += 1;
                        }

                    }

                }

                return niveau;
            }
        }
        return -1; // si y'a eu une erreur
    }

    /**
     * Renvoie la liste de tout les niveaus des enseignements d'un parcours dont on connais le nom
     * @param restaureParcours (ArrayList&lt;Parcours&gt;)
     * @param nomParcours (String)
     * @return (ArrayList&lt;Integer&gt;)
     */
    public static ArrayList<Integer> niveauEnseignementParcours(ArrayList<Parcours> restaureParcours, String nomParcours) {
        if (!restaureParcours.isEmpty()) {
            ArrayList<Enseignement> ueParcours = Parcours.listeEnsParcoursSansMention(restaureParcours, nomParcours);
            ArrayList<Integer> listeNiveau = toutNiveauEnseignement(ueParcours);
            return listeNiveau;
        }
        return null;
    }

    /**
     * Renvoie une arraylist d'entier correspondant au niveau de chaque
     * enseignement
     *
     * @param restaure
     * @return
     */
    public static ArrayList<Integer> toutNiveauEnseignement(ArrayList<Enseignement> restaure) {
        if (!restaure.isEmpty()) {
            ArrayList<Integer> listeNiveau = new ArrayList<>();
            for (Enseignement ue : restaure) {
                listeNiveau.add(niveauEnseignement(restaure, ue));
            }
            return listeNiveau;
        }
        return null;
    }

    /**
     * Renvoie une arraylist d'entier correspondant au niveau de chaque
     * enseignement sous forme clé = Enseignement, valeur = Niveau
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @return (HashMap&lt;Enseignement,Integer&gt;)
     */
    public static HashMap<Enseignement, Integer> toutNiveauEnseignementHashMap(ArrayList<Enseignement> restaure) {
        if (!restaure.isEmpty()) {
            HashMap<Enseignement, Integer> listeNiveau = new HashMap<>();
            for (Enseignement ue : restaure) {
                listeNiveau.put(ue, niveauEnseignement(restaure, ue));
            }
            return listeNiveau;
        }
        return null;
    }

    /**
     * Renvoie la liste des Enseignement par niveau choisis
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param niveauChoisis (int)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public static ArrayList<Enseignement> listeParNiveauChoisis(ArrayList<Enseignement> restaure, int niveauChoisis) {
        if (!restaure.isEmpty()) {
            HashMap<Enseignement, Integer> listeNiveauTout;
            listeNiveauTout = Enseignement.toutNiveauEnseignementHashMap(restaure);
            if (!listeNiveauTout.isEmpty()) {
                ArrayList<Enseignement> listeChoix = new ArrayList<>();
                for (Enseignement ue : listeNiveauTout.keySet()) {
                    if (listeNiveauTout.get(ue).equals(niveauChoisis)) {
                        listeChoix.add(ue);
                    }
                }
                return listeChoix;
            }

        }
        return null;

    }

    private static String getPath() {
        String path = Enseignement.class
                .getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar   || verif[verif.length - 1].equals("dist")
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Enseignement.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE ENSEIGNEMENT: "+absolutePath);
        return absolutePath;

    }

    /**
     * Permet de sauvegarder un Enseignement dans un fichier enseignements.csv
     * Si une modification à lieu la ligne concerné est modifier
     */
    public void sauvegarde() {
        StringBuilder sb = new StringBuilder();
        StringBuilder tout = new StringBuilder();
        boolean dejaDansFich = false, uneModification = false;
        int cpt = 0;
        String[] colonne;
        String path = getPath();
        // Vérifie si le répertoire Données existe sinon le crée
        File directory = new File(path + "Données");
        if (!directory.exists()) {
            directory.mkdir();
        }

        // Vérifie si le fichier existe si ce n'est pas le cas initialise la premiere ligne
        File f = new File(path + CHEMINDEFAULT);
        if (!f.exists()) {
            if (!f.isDirectory()) {
                sb.append('\ufeff');
                sb.append("Nom");
                sb.append(";");
                sb.append("Code");
                sb.append(';');
                sb.append("Credit");
                sb.append(';');
                sb.append("Prerequis");
                sb.append('\n');
            }
        }
        //Rajout de la ligne
        sb.append(nom);
        sb.append(';');
        sb.append(code);
        sb.append(';');
        sb.append(credit);
        sb.append(';');
        if (prerequis.isEmpty()) {
            sb.append("[]");
        } else {
            // Permet d'avoir un format plus facile à traiter
            for (int i = 0; i < prerequis.size(); i++) {
                sb.append("[");
                sb.append(prerequis.get(i).getCode());
                sb.append(" , ");
                sb.append(prerequis.get(i).getCredit());
                sb.append("]");
            }
        }

        // On va lire ligne par ligne le fichier 
        BufferedReader br;

        if (f.exists()) {
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(path + CHEMINDEFAULT), "UTF-8"));
                String ligne;
                // Tant que la ligne n'est pas vide

                while ((ligne = br.readLine()) != null) {
                    colonne = ligne.split(";");
                    if (ligne.equals(sb.toString())) {
                        dejaDansFich = true;
                    } else {
                        if (colonne[1].equals(code)) {
                            //System.out.println("ligne a modif");
                            tout.append(sb.toString());
                            tout.append("\n");
                            dejaDansFich = true;
                            uneModification = true;

                        } else {
                            for (int i = 0; i < prerequis.size(); i++) {
                                if (colonne[1].equals(prerequis.get(i).getCode())) {
                                    cpt++;
                                }
                            }
                            //System.out.println("fin des conditions ");
                            tout.append(ligne);
                            tout.append("\n");

                        }

                    }
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Enseignement.class
                        .getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {
                Logger.getLogger(Enseignement.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (cpt != prerequis.size()) {
            System.out.println("Sauvegarder d'abord les prérequis de cette enseignements ! ");
            return;
        }

        // Enregistrement dans le fichier
        if (!dejaDansFich) {
            try {
                //FileWriter writer = new FileWriter(path + CHEMINDEFAULT;
                OutputStream out = new FileOutputStream(path + CHEMINDEFAULT, false);
                try (Writer writer = new OutputStreamWriter(out, "UTF-8")) {
                    sb.append("\n");
                    tout.append(sb.toString());
                    writer.append(tout);
                    writer.flush();
                    //System.out.println("done!");

                }

            } catch (IOException ex) {
                Logger.getLogger(Enseignement.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (dejaDansFich) {
                if (!uneModification) {
                    //System.out.println("Déjà dans le fichier ! ");
                } else {
                    if (uneModification) {
                        try {
                            OutputStream out = new FileOutputStream(path + CHEMINDEFAULT);
                            try (Writer writer = new OutputStreamWriter(out, "UTF-8")) {
                                writer.append(tout);
                                writer.flush();
                                //System.out.println("done!");;;

                            }

                        } catch (IOException ex) {
                            Logger.getLogger(Enseignement.class
                                    .getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        }

    }

    /**
     * Permet de restaurer les Enseignements en récupérant une ArrayList
     * contenant tout les Enseignements sauvegarder dans le fichier
     * enseignements.csv
     *
     * @return ArrayList&lt;Enseignement&gt;
     */
    public static ArrayList<Enseignement> restaure() {
        return Enseignement.restaure(getPath() + CHEMINDEFAULT);
    }

    /**
     * Retourne une ArrayList&lt;Enseignementn&gt; contenue dans le fichier dont
     * le chemin et donner en paramètre !! les slashs doivent être doublée.
     *
     * @param chemin (String sous forme C:\\lechemin\\estvoila\\fichier.csv)
     * @return ArrayList&lt;Enseignementn&gt;
     */
    public static ArrayList<Enseignement> restaure(String chemin) {

        File f = new File(chemin);
        BufferedReader br;
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    br = new BufferedReader(new InputStreamReader(new FileInputStream(chemin), "UTF-8")); // lis ligne par ligne
                    String ligne, sansPare;
                    String[] colonne;
                    String[] verif;
                    boolean formatCorrect = false; // doit valoir true pour que la restauration ce fasse

                    ArrayList<Enseignement> liste = new ArrayList<>();

                    while ((ligne = br.readLine()) != null) {
                        //System.out.println("ligne :"+ligne);
                        colonne = ligne.split(";");

                        //transforme les [] en "" 
                        sansPare = colonne[3];
                        sansPare = sansPare.substring(1); // enleve le premier [
                        sansPare = sansPare.substring(0, sansPare.length() - 1);// enleve le dernier ]

                        if ((!colonne[0].equals("Nom") || !colonne[0].equals("\ufeffNom") || !colonne[0].equals("ï»¿Nom")) && !colonne[1].equals("Code")
                                && !colonne[2].equals("Credit") && !colonne[3].equals("Prerequis") && formatCorrect) {

                            liste.add(new Enseignement(colonne[0], colonne[1], Integer.parseInt(colonne[2])));
                            if (!"".equals(sansPare)) {
                                // il y a un prerequis a ajouter
                                verif = sansPare.split("]");
                                for (int i = 0; i < liste.size(); i++) {
                                    for (int j = 0; j < verif.length; j++) {
                                        if (liste.get(i).getCode().equals(verif[j].split(",")[0].trim())) {
                                            liste.get(liste.size() - 1).addPrerequis(liste.get(i));
                                        } else {
                                            if (liste.get(i).getCode().equals(verif[j].split(",")[0].trim().substring(1))) {
                                                liste.get(liste.size() - 1).addPrerequis(liste.get(i));
                                            }
                                        }

                                    }

                                }
                                // Si il n'y a pas le meme nombre de prerequis 
                                for (int cpt = 0; cpt < verif.length; cpt++) {
                                    if ((verif.length) != (liste.get(liste.size() - 1).getPrerequis().size())) {

                                        if (verif[cpt].charAt(0) == '[') {
                                            liste.get(liste.size() - 1).addPrerequis(
                                                    new Enseignement(
                                                            verif[cpt].split(",")[0].trim().substring(1),
                                                            verif[cpt].split(",")[1].trim(),
                                                            Integer.parseInt(verif[cpt].split(",")[2].trim())));
                                        } else {
                                            liste.get(liste.size() - 1).addPrerequis(
                                                    new Enseignement(
                                                            verif[cpt].split(",")[0].trim(),
                                                            verif[cpt].split(",")[1].trim(),
                                                            Integer.parseInt(verif[cpt].split(",")[2].trim())));
                                        }

                                    }

                                }

                            }

                        }
                        if ((colonne[0].equals("\ufeffNom") || colonne[0].equals("Nom") || colonne[0].equals("ï»¿Nom")) && colonne[1].equals("Code")
                                && colonne[2].equals("Credit") && colonne[3].equals("Prerequis")) {
                            formatCorrect = true;
                        }

                    }
                    if (!formatCorrect) {
                        //System.out.println("Le format des colonnes n'est pas correcte !");
                        return null;
                    }
                    return liste;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Enseignement.class
                            .getName()).log(Level.SEVERE, null, ex);

                } catch (IOException ex) {
                    Logger.getLogger(Enseignement.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            //System.out.println("Fichier n'existe pas");
            return null;
        }
        //System.out.println("Ce n'est pas un fichier");
        return null;

    }

    @Override
    public String toString() {
        return nom;
    }

}
