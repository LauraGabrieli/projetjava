package gestion;

import java.util.Calendar;

/**
 * Décrit une Année Universitaire
 * 
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class AnneeUniv {

    private String annee;

    /**
     * Semestre 2
     */
    public static final String PAIR = "S2";

    /**
     * Semestre 1
     */
    public static final String IMPAIR = "S1";

    /**
     * Définit une annee univ
     */
    public AnneeUniv() {
        Calendar c = Calendar.getInstance();
        //Création de l'année universitaire
        if (c.get(Calendar.MONTH) > 6) {
            this.annee = String.valueOf(c.get(Calendar.YEAR)) + "/" + String.valueOf(c.get(Calendar.YEAR) + 1);
        } else {
            this.annee = String.valueOf(c.get(Calendar.YEAR) - 1) + "/" + String.valueOf(c.get(Calendar.YEAR));
        }
    }

    /**
     * Attention !!! Constructeur pour la restauration UNIQUEMENT !!
     *
     * @param nomAn (String)
     */
    public AnneeUniv(String nomAn) {
        this.annee = nomAn;
    }

    /**
     * Permet de récupérer l'année d'une AnneeUniv
     *
     * @return annee
     */
    public String getAnnee() {
        return this.annee;
    }

    /**
     * Permet de récupérer le semestre en cours d'une AnneeUniv
     *
     * @return semestre
     */
    public String getSemestreEnCours() {
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.MONTH) > 8) {
            return IMPAIR;
        } else {
            if (c.get(Calendar.MONTH) < 7) {
                return PAIR;
            } else {
                return null;
            }
        }
    }

    /**
     * Permet de récupérer le semestre pair d'une AnneeUniv
     *
     * @return pair
     */
    public String getPAIR() {
        return PAIR;
    }

    /**
     * Permet de récupérer le semestre impair d'une AnneeUniv
     *
     * @return impair
     */
    public String getIMPAIR() {
        return IMPAIR;
    }

    @Override
    public String toString() {
        return "AnneeUniv{" + "annee=" + annee + ", 1er semestre=" + IMPAIR + ", 2eme semestre=" + PAIR + '}';
    }

    public boolean equals(AnneeUniv a) {
        return (this.annee.equals(a.getAnnee())) && (IMPAIR.equals(a.getIMPAIR())) && PAIR.equals(a.getPAIR());
    }

}
