package gestion;

/**
 * Décrit le Suivi d'un étudiant
 * 
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Suivi {

    private boolean valide;
    private Enseignement ens;
    private AnneeUniv an;
    private String semestre;

    /**
     * Définit un Suivi pour l'Enseignement
     *
     * @param valide (boolean)
     * @param ens (Enseignement)
     * @param an (AnneeUniv)
     */
    public Suivi(boolean valide, Enseignement ens, AnneeUniv an) {
        this.valide = valide;
        this.ens = ens;
        this.an = an;
        this.semestre = an.getSemestreEnCours();
    }

    /**
     * Attention !! Constructeur pour la restauration uniquement !!
     *
     * @param valide (boolean)
     * @param ens (Enseignement)
     * @param an (AnneeUniv)
     * @param sem (String)
     */
    public Suivi(boolean valide, Enseignement ens, AnneeUniv an, String sem) {
        this.valide = valide;
        this.ens = ens;
        this.an = an;
        this.semestre = sem;
    }

    /**
     * Permet de récupérer valide d'un Suivi
     *
     * @return true si il a valider l'Enseignement ou false sinon
     */
    public boolean getValide() {
        return valide;
    }

    /**
     * Permet de récupérer un Enseignement d'un Suivi
     *
     * @return Enseignement
     */
    public Enseignement getEnseignement() {
        return ens;
    }

    /**
     * Permet de récupérer une Anne
     *
     * @return AnneeUniv
     */
    public AnneeUniv getAnnee() {
        return an;
    }

    /**
     * Permet de récupérer un semestre
     *
     * @return String
     */
    public String getSemestre() {
        return this.semestre;
    }

    /**
     * Permet de définir valide d'un Suivi
     *
     * @param valide (boolean)
     */
    public void setValide(boolean valide) {
        this.valide = valide;
    }

    @Override
    public String toString() {
        return "Suivi{" + " UE=" + ens + ", annee=" + an + ", semestre=" + semestre + "valide=" + valide + '}';
    }

}
