package gestion;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Les tests avant incorporation dans la partie graphique.
 *
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Test {

    public static void main(String[] args) {

        // Vite illisisble a cause des toString mais permet de constater que ça marcher
        // J'ai mis dans un sous dossier comme ça pour le moment ont peux faire des petits tests avec
        // les données qu'on avait deja mais il conviendrait de les mettres en données principale pour verifier 
        // qu'il n'y a pas de probleme + ça remet un exemple pour les restauration avec chemin pour Laura 
        ArrayList<Enseignement> listeEns = Enseignement.restaure("Données\\TEST\\enseignements.csv");
        System.out.println("Liste enseignements restaurer : " + listeEns);
        ArrayList<Mention> listeMention = Mention.restaure(listeEns, "Données\\TEST\\mentions.csv");
        System.out.println("Liste mention restaurer : " + listeMention);
        ArrayList<Parcours> listeParcours = Parcours.restaureP(listeEns, listeMention, "Données\\TEST\\parcours.csv");
        System.out.println("Liste parcours restaurer : " + listeParcours);
        ArrayList<Etudiant> listeEtu = Etudiant.restaure(listeEns, listeParcours, "Données\\TEST\\etudiants.csv");
        System.out.println("Liste etudiants restaurer : " + listeEtu);
        System.out.println(Etudiant.listeEtuNom(listeEtu, "Prunevielle"));

        // System.out.println("test: ");
        // ArrayList<Integer> nbEtuSuisEns = Enseignement.nbEtuSuisEns(listeEns, listeEtu);
        //System.out.println("nb etu par ens:" + nbEtuSuisEns);
        //System.out.println("nb: " + nbEtuSuisEns.size());
        // System.out.println("liste ens " + listeEns.size());
        //HashMap<Enseignement,Integer> okay =Enseignement.nbEtuSuisEnsHashMap(listeEns,listeEtu);
        // System.out.println("liste hash :"+okay);
        //System.out.println("okay size :"+okay.size());
        System.out.println("Autre test ------");
        System.out.println("nomEnseignementPourMention :" + Mention.nomEnseignementPourMention(listeMention, listeParcours, "MIASHS"));
        // System.out.println("test : " + Mention.nomEnseignementPourMention(listeMention, listeParcours, "MIASHS").size());

        System.out.println("codeEnseignementPourMention :" + Mention.codeEnseignementPourMention(listeMention, listeParcours, "MIASHS"));
        // System.out.println("test : " + Mention.codeEnseignementPourMention(listeMention, listeParcours, "MIASHS").size());

        System.out.println("nomEnseignementPourParcours :" + Parcours.nomEnseignementPourParcours(listeParcours, "MIAGE"));
        // System.out.println("test :" + Parcours.nomEnseignementPourParcours(listeParcours, "MIAGE").size());

        System.out.println("codeEnseignementPourParcours :" + Parcours.codeEnseignementPourParcours(listeParcours, "MIAGE"));
        // System.out.println("test :" + Parcours.codeEnseignementPourParcours(listeParcours, "MIAGE").size());

        /*  System.out.println("TEST NIVEAU ---------");
        System.out.println(listeEns.get(97));
        System.out.println(Enseignement.niveauEnseignement(listeEns,listeEns.get(97)));
        System.out.println("");
        System.out.println("Niveau de tout les ens :");
        ArrayList<Integer> listeNiveau =Enseignement.toutNiveauEnseignement(listeEns);
        System.out.println(listeNiveau);
        System.out.println(listeNiveau.size());
        HashMap<Enseignement,Integer> listeNiveauHash = Enseignement.toutNiveauEnseignementHashMap(listeEns);
        System.out.println("liste niveau hashmap :"+listeNiveauHash);
        
        System.out.println("liste par niveau ici nv 0: "+Enseignement.listeParNiveauChoisis(listeEns, 0));
        System.out.println("Liste par niveau ici nv 3: "+Enseignement.listeParNiveauChoisis(listeEns, 3));
        
        // listeEns.get() retourne un enseignement de la liste restaurer 
        System.out.println("Nb d'étudiant pour un ens: "+Enseignement.nbEtuSuisUnEns(listeEtu, listeEns.get(22)));
        
         */
        // post reu 27
        // AVEC les ens + le code
        System.out.println("Niveau ens :" + Enseignement.niveauEnseignementCode(listeEns, listeEns.get(22).getCode()));
        // Avec les restaures de ens + parcours + string nom du parcours
        System.out.println("Niveaux possible pour parcours : " + Enseignement.niveauEnseignementParcours(listeParcours, "MIAGE"));
        //test taux de reussite
        System.out.println("test : " + listeEns.get(0).getCode());
        //  System.out.println("etu test "+ listeEtu.get(listeEtu.size()-1));
        System.out.println("taux : " + Enseignement.tauxDeReussiteEns(listeEtu, listeEns, listeEns.get(0)));
        /*for(Enseignement ens : listeEns){
            System.out.print(ens.getCode()+" : ");
            System.out.println("Taux : "+Enseignement.tauxDeReussiteEns(listeEtu, listeEns, ens)); 
        }   */

        // prevu?
        System.out.println("\n");
        System.out.println("-------------------");
        System.out.println("Previ");

        System.out.println("test : " + listeEns.get(85).getCode());
        System.out.println(Enseignement.nbEtuSuisUnEnsPrevisionnel(listeEtu, listeEns, listeEns.get(85)));

        /*  for(Enseignement ens : listeEns){
            System.out.print(ens.getCode()+" : ");
            System.out.println(Enseignement.nbEtuSuisUnEnsPrevisionnel(listeEtu, listeEns, ens));
            
        }*/
        System.out.println("test des noms des mentions pour ue et parcours");

        for (Enseignement ens : listeEns) {
            System.out.println("Mention de l'UE : " + ens.getCode() + " = " + Parcours.mentionPourUE(listeMention, listeParcours, ens));

        }
        System.out.println(" test parcours :");
        for (Enseignement ens : listeEns) {
            System.out.println("Parcours de l'UE : " + ens.getCode() + " = " + Parcours.parcoursPourUE(listeMention, listeParcours, ens));

        }

        //System.out.println("Prévu : "+Enseignement.nbEtuSuisUnEnsPrevisionnel(listeEtu,listeEns, listeEns.get(29)));
// Restauration Enseignement        
        /*    ArrayList<Enseignement> listeEns = Enseignement.restaure();
        System.out.println("Liste enseignement restaurer : " + listeEns);*/
//        ArrayList<Enseignement> listeEnsChemin = Enseignement.restaure(
//                "C:\\Users\\CarlosMiguelCor\\Desktop\\donneestest\\enseignements.csv"
//        ); // Ne marchera pas chez vous mais chez moi non plus car mauvais chemin donc renvoie liste null
//        System.out.println("listeEnsChemin : " + listeEnsChemin);
//        //exemple
//        System.out.println(listeEns.get(3).getCode());
//        System.out.println(listeEns.get(3).getPrerequis());
        //Restauration Mention
        /*  ArrayList<Mention> listeMention = Mention.restaure(listeEns);
        System.out.println("Liste mention restaurer : " + listeMention);*/
//        ArrayList<Mention> listeMentionChemin = Mention.restaure(
//                "C:\\Users\\CarlosMiguelCorreiaD\\Desktop\\donneestest\\mentions.csv");// Marche chez moi mais marchera pas chez vous
//        System.out.println("listeMentionChemin : " + listeMentionChemin);
//        System.out.println(listeMention.get(0).getNom());
//        System.out.println(listeMention.get(0).getEnseignements());
        // Restauration Parcours !!! J'ai changer la restauration de Parcours il faut mettre restaureP 
        // j'ai pas trouver mieux comme solution
        /*    ArrayList<Parcours> listeParcours = Parcours.restaureP(listeEns, listeMention);
        System.out.println("Liste parcours restaurer : " + listeParcours);*/
//        ArrayList<Parcours> listeParcoursChemin = Parcours.restaureP(
//                "C:\\Users\\CarlosMiguelCorreiaD\\Desktop\\donneestest\\parcours.csv"
//        );
//        System.out.println("ListeParcoursChemin : " + listeParcoursChemin);
//        System.out.println(listeParcours.get(0).getNom());
//        System.out.println(listeParcours.get(0).getEnseignements());
//        System.out.println(listeParcours.get(0).getEnseignementsP());
        // Restauration etudiant
        /*  ArrayList<Etudiant> listeEtu = Etudiant.restaure(listeEns, listeParcours);*/
        //System.out.println("listeEtu "+ listeEtu.get(3));
        //Création année universitaire
        // AnneeUniv a = new AnneeUniv();
        //AnneeUniv annee2019 = new AnneeUniv("2019/2020"); // pour test avec etu oceane
        //création enseignement
        /*  Enseignement jav0 = new Enseignement("JAV0", "L31", 3);
        Enseignement java1 = new Enseignement("JAV1", "L32", 6);
        Enseignement droit = new Enseignement("DROIT", "L33", 3);
        Enseignement math = new Enseignement("MATHS", "L34", 3);
        Enseignement math2 = new Enseignement("MATHS2", "L35", 3);
        Enseignement ro = new Enseignement("RO", "L36", 3); 

        java1.addPrerequis(jav0);
        java1.addPrerequis(math);
        droit.addPrerequis(math);
        math2.addPrerequis(math);
        math2.addPrerequis(java1);
        ro.addPrerequis(math);*/
        //Création mention
        /* Mention miashs = new Mention("MIASHS");
        Mention eco = new Mention("ECO");
        miashs.ajoutEnseignement(jav0);
        miashs.ajoutEnseignement(math);
        miashs.ajoutEnseignement(java1);
        eco.ajoutEnseignement(jav0);
        eco.ajoutEnseignement(droit);

        //création parcours
        Parcours miage = new Parcours("MIASHS", miashs.getEnseignements(), "MIAGE");
        Parcours mathe = new Parcours("MIASHS", miashs.getEnseignements(), "Mathematiques");
        Parcours stats = new Parcours("MIASHS", miashs.getEnseignements(), "STATS ET INFO DECI");
        Parcours cogni = new Parcours("MIASHS", miashs.getEnseignements(), "COGNITIVES");

        Parcours ecoDroit = new Parcours("ECO", eco.getEnseignements(), "DROIT");
        miage.ajoutEnseignement(droit);
        miage.ajoutEnseignement(ro);
        stats.ajoutEnseignement(ro);*/
        //Création étudiant
        //Etudiant e = new Etudiant(144, "Milet", "Claire", listeParcours.get(3));
        // e.sauvegarde();
        /*  Etudiant laura = new Etudiant(2, "Gabrieli", "Laura", miage);
        Etudiant paul = new Etudiant(3, "Simon", "Paul", ecoDroit);
        Etudiant oceane = new Etudiant(4, "Prunevielle", "Oceane", miage);
        Etudiant oceane2 = new Etudiant(5, "Prunevielle", "Manon", ecoDroit); // la soeur jumelle d'oceane (ou presque)
        Etudiant carlos = new Etudiant(6, "Correia", "Carlos", mathe);
        Etudiant pierre = new Etudiant(7, "Toto", "Pierre", stats);
        Etudiant jack = new Etudiant(8, "Sparrow", "jack", cogni);
         */
 /*Suivi s1 = new Suivi(true, math2, annee2019, "S1"); // pour test pour la restauration
        oceane.ajoutSuivi(s1);
        // faudrait peut-être rajouter ça comme methode dans etudiant pour set un enseignement en valide
        for (int i = 0; i < oceane.getListeSuivi().size(); i++) {
            if (oceane.getListeSuivi().get(i).getEnseignement().equals(math)) {
                oceane.getListeSuivi().get(i).setValide(true);
                System.out.println(oceane.getListeSuivi().get(i));
            }

        }

        //ajout de l'enseignement dans le suivi de l'etudiant (pb car l'enseignement est déjà suivi)
        e.ajouterEnseignement(jav0); // Problème résolu ???? -Carlos / Oui -Océane
         */
        //Traitements
        /*   System.out.println("Année : " + a);
        System.out.println("Semestre en cours : " + a.getSemestreEnCours());
//        System.out.println("Java : " + Enseignement.ensUnique(listeEns, "COO"));
        System.out.println("Miashs : " );
        for (int i=0; i<listeMention.size(); i++){
            if (listeMention.get(i).getNom().equals ("MIASHS")){
                System.out.println(listeMention.get(i));
            }
        }
        System.out.println("Miage : ");
        for (int i=0; i<listeParcours.size(); i++){
            if (listeParcours.get(i).getNomP().equals ("MIAGE")){
                System.out.println(listeParcours.get(i));
            }
        }*/
 /*System.out.println("Etudiant : " + listeEtu.get(12));
        for (int i = 0; i < listeEtu.get(12).ueEnCours().size(); i++) {
            System.out.print(listeEtu.get(12).ueEnCours().get(i) + " ");
        }
        System.out.println();*/
        //Grooooooooos bug si les fichiers de départ n'existent pas 
        //(NullPointerException partout à cause de listeMention qui est vide 
        //donc m est null et m.size() bugge tout
        //Mais fonctionne si les fichiers sont remplis
        /*       ArrayList<String> m = Mention.nomMentions(listeMention);
        if (m != null) { // le if resout le bug que t'avais -carlos
            System.out.println("Mentions :");
            for (int i = 0; i < m.size(); i++) {
                System.out.println(m.get(i));
            }

        }*/
        //sauvegarde d'enseignement
        /*jav0.sauvegarde();
        droit.sauvegarde();
        math.sauvegarde();
        java1.sauvegarde();
        math2.sauvegarde();
        ro.sauvegarde();*/
        //sauvegarde de mention
        /*miashs.sauvegarde();
        eco.sauvegarde();*/
        //sauvegarde de parcours
        /*miage.sauvegarde();
        ecoDroit.sauvegarde();
        mathe.sauvegarde();
        stats.sauvegarde();
        cogni.sauvegarde();*/
        //sauvegarde de etudiant
        /*e.sauvegarde();
        laura.sauvegarde();
        paul.sauvegarde();
        oceane.sauvegarde();
        oceane2.sauvegarde();
        carlos.sauvegarde();
        pierre.sauvegarde();
        jack.sauvegarde();*/
        // Recuperation des listes (pour t'aider Laura) (on peut supprimer après ça pas d'utilité
        /*System.out.println(Etudiant.listeEtuNom(listeEtu, "Prunevielle"));
        System.out.println(Etudiant.listeEtuNom(listeEtu, "Prunevielle").get(0));
        System.out.println(Etudiant.listeEtuNom(listeEtu, "Prunevielle").get(1));

        System.out.println("Liste des etudiants ayant numE = 1 ");
        System.out.println(Etudiant.listeEtuNumE(listeEtu, 1));
        System.out.println("Liste des etudiants ayant comme prenom = Laura ");
        System.out.println(Etudiant.listeEtuPrenom(listeEtu, "Laura"));
        System.out.println("Liste des etudiants ayant pour parcours MIAGE");
        ArrayList<Etudiant> listeEtuParcoursTest = Etudiant.listeEtuParcours(listeEtu,"MIAGE");
        for(Etudiant etu :listeEtuParcoursTest){
            System.out.println(etu);
        }
        System.out.println("Liste des etudiant ayant pour mention miashs ");
         ArrayList<Etudiant> listeEtuMentionTest = Etudiant.listeEtuMention(listeEtu,"MIASHS");
        for(Etudiant etu :listeEtuMentionTest){
            System.out.println(etu);
        }
        System.out.println("Enseignement possible pour un etudiant donne ; ");
        System.out.println(oceane.listeEnsPossible(listeEns));
        
        System.out.println("Liste des enseignements ayant comme nom = JAV0 ; ");
        System.out.println(Enseignement.ensUnique(listeEns, "JAV0"));
        
        System.out.println("Liste des enseignements pour le parcours MIAGE ; ");
        System.out.println(Parcours.listeEnsParcours(listeParcours, "MIAGE"));
        
        System.out.println("Liste des enseignements pour la mention MIASHS ; ");
        System.out.println(Mention.ensMention(listeMention, "MIASHS"));
        
        System.out.println("Liste des étudiants qui suivent JAV0 ; ");
        System.out.println(Etudiant.listeEtudEns(listeEtu, "JAV0"));
        
        System.out.println("Liste des parcours de la mention MIASHS ; ");
        System.out.println(Parcours.nomParcoursMention(listeParcours, "MIASHS"));
        
        System.out.println("Liste des noms de tous les UE ; ");
        System.out.println(Enseignement.listeNomEns(listeEns));*/
 /*        System.out.println("liste possible :"+ listeEtu.get(62).listeEnsPossible(listeEns));
        // J'ai rajouter ces 2 methodes la : 
        System.out.println("liste ens possible mention :"+listeEtu.get(62).listeEnsPossibleMention(listeEns,listeMention,listeParcours,"MIASHS"));
        System.out.println("liste ens possible parcours :"+listeEtu.get(62).listeEnsPossibleParcours(listeEns,listeParcours,"MIAGE"));
        System.out.println("Liste des étudiants qui suivent Methode de developpement structure : ");
        System.out.println(Etudiant.listeEtudEns(listeEtu, "Methode de developpement structure"));
        System.out.println(Parcours.listeEnsMention(listeParcours, "MIASHS"));
        System.out.println(Parcours.listeEnsParcoursSansMention(listeParcours, "MIAGE"));
        System.out.println(listeEns.get(84).isDecouverte());*/
 /* System.out.println("okay ???");
        ArrayList<Etudiant> pourLeTest = Etudiant.listeEtudCode(listeEtu,listeEns,"EDSC01");
        for(int i=0;i<pourLeTest.size();i++){
             System.out.println("num : "+pourLeTest.get(i).getNumE()+" "+pourLeTest.get(i).getNom());
        }*/
    }
}
